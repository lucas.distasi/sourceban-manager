package net.vidyagaems.sourcebanmanager.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    // Commands
    public static final String SBM_COMMAND = "!sbm";

    // Parameters
    public static final String BANS_PARAMETERS = "bans";
    public static final String COMMS_PARAMETERS = "comms";
    public static final String STAFF_PARAMETERS = "staff";
    public static final String VIPS_PARAMETERS = "vips";
    public static final String HELP_PARAMETERS = "help";

    // Roles
    public static final String VIP = "VIP";
    public static final String MOD = "MOD";
    public static final String ADMIN = "ADMIN";
    public static final String ROOT = "ROOT";

    // Defaults
    public static final Integer ORANGE_ID = 2;
    public static final String VIP_DEFAULT_TAG = "VIP";
    public static final String MOD_DEFAULT_TAG = "MOD";
    public static final String MOD_DEFAULT_TAG_NAME_COLOR = "FFA500";
    public static final String ADMIN_DEFAULT_TAG = "Vidya";
    public static final String ADMIN_DEFAULT_TAG_NAME_COLOR = "3BB9FF";
    public static final String SUCCESS_REACTION = "U+2705";
    public static final String ERROR_REACTION = "U+274C";

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class PropertiesConstants {
        public static final String BANS_INFO_PROPERTY = "bans-info";
        public static final String COMMS_INFO_PROPERTY = "comms-info";
        public static final String STAFF_INFO_PROPERTY = "staff-info";
        public static final String VIPS_INFO_PROPERTY = "vips-info";
        public static final String ADD_BAN_PROPERTY = "add-ban";
        public static final String VIEW_BAN_PROPERTY = "view-ban";
        public static final String UPDATE_BAN_PROPERTY = "update-ban";
        public static final String REMOVE_BAN_PROPERTY = "remove-ban";
        public static final String ADD_COMM_BAN_PROPERTY = "add-comm-ban";
        public static final String VIEW_COMM_BAN_PROPERTY = "view-comm-ban";
        public static final String UPDATE_COMM_BAN_PROPERTY = "update-comm-ban";
        public static final String REMOVE_COMM_BAN_PROPERTY = "remove-comm-ban";
        public static final String ADD_ADMIN_PROPERTY = "add-admin";
        public static final String VIEW_ADMIN_PROPERTY = "view-admin";
        public static final String UPDATE_ADMIN_PROPERTY = "update-admin";
        public static final String REMOVE_ADMIN_PROPERTY = "remove-admin";
        public static final String RENEW_ADMIN_PROPERTY = "renew-admin";
        public static final String ADD_MOD_PROPERTY = "add-mod";
        public static final String VIEW_MOD_PROPERTY = "view-mod";
        public static final String UPDATE_MOD_PROPERTY = "update-mod";
        public static final String REMOVE_MOD_PROPERTY = "remove-mod";
        public static final String RENEW_MOD_PROPERTY = "renew-mod";
        public static final String ADD_VIP_PROPERTY = "add-vip";
        public static final String VIEW_VIP_PROPERTY = "view-vip";
        public static final String UPDATE_VIP_PROPERTY = "update-vip";
        public static final String REMOVE_VIP_PROPERTY = "remove-vip";
        public static final String RENEW_VIP_PROPERTY = "renew-vip";
        public static final String INVALID_PARAMETER_PROPERTY = "invalid-parameter";
        public static final String INVALID_USERNAME_STEAM_ID = "invalid-username-steam-id";
        public static final String INVALID_STEAM_ID = "invalid-steam-id";
        public static final String INVALID_USERNAME = "invalid-username";
        public static final String USERNAME_EXISTS = "username-exists";
        public static final String INVALID_SERVER = "invalid-server";
        public static final String INVALID_TAG_NAME = "invalid-tag-name";
        public static final String INVALID_HEX_COLOR = "invalid-hex-color";
        public static final String INVALID_DISCORD_ID = "invalid-discord-id";
        public static final String INVALID_MONTHS = "invalid-months";
        public static final String COMMANDS_TITLE_PROPERTY = "help-commands-title";
        public static final String SBM_COMMANDS_HELP = "sbm-commands-help";
        public static final String PARAMETERS_TITLE_PROPERTY = "help-parameters-title";
        public static final String STEAMID_PARAMETER = "steamid-parameter";
        public static final String SERVER_PARAMETER = "server-parameter";
        public static final String TAGNAME_PARAMETER = "tagname-parameter";
        public static final String TAGCOLOR_PARAMETER = "tagcolor-parameter";
        public static final String DAYS_PARAMETER = "days-parameter";
        public static final String STEAMID_PARAMETER_DESCRIPTION = "steamid-parameter-description";
        public static final String SERVER_PARAMETER_DESCRIPTION = "server-parameter-description";
        public static final String TAGNAME_PARAMETER_DESCRIPTION = "tagname-parameter-description";
        public static final String TAGCOLOR_PARAMETER_DESCRIPTION = "tagcolor-parameter-description";
        public static final String DAYS_PARAMETER_DESCRIPTION = "days-parameter-description";
        public static final String PLAYER_STEAM_ID_QUESTION = "player-steam-id-question";
        public static final String PLAYER_SERVER_QUESTION = "player-server-question";
        public static final String PLAYER_TAG_NAME_QUESTION = "player-tag-name-question";
        public static final String PLAYER_TAG_COLOR_QUESTION = "player-tag-color-question";
        public static final String PLAYER_DISCORD_ID_QUESTION = "player-discord-id-question";
        public static final String PLAYER_IS_NOT_IN_DISCORD_SERVER = "player-not-in-discord-server";
        public static final String PLAYER_DURATION_QUESTION = "player-duration-question";
        public static final String PLAYER_USERNAME_STEAM_ID_QUESTION = "player-username-steam-id-question";
        public static final String PLAYER_USERNAME_QUESTION = "player-username-question";
        public static final String EVENT_WAITER_TIMEOUT = "event-waiter-timeout";
        public static final String ADD_PLAYER_SUCCESSFULLY = "add-player-successfully";
        public static final String ADD_PLAYER_ERROR = "add-player-error";
        public static final String ADD_PLAYER_SERVER_GROUP_SUCCESSFULLY = "add-player-servergroup-successfully";
        public static final String ADD_PLAYER_SERVER_GROUP_ERROR = "add-player-servergroup-error";
        public static final String ADD_PLAYER_CHAT_COLORS_SUCCESSFULLY = "add-chatcolors-successfully";
        public static final String ADD_PLAYER_CHAT_COLORS_ERROR = "add-chatcolors-error";
        public static final String PLAYER_EXISTS_WARNING = "player-exists-warning";
        public static final String PLAYER_CHATCOLOR_EXISTS_WARNING = "player-chatcolor-exists-warning";
        public static final String ERROR_NO_SERVER_GROUP_FOUND = "error-no-server-group-found";
        public static final String PLAYER_INFORMATION = "player-information";
        public static final String PLAYER_ALREADY_HAS_ROLE_IN_SERVER = "player-already-has-role-in-server";
        public static final String NO_ROLE_FOR_PLAYER = "no-role-for-player";
        public static final String PLAYER_SERVER_DELETION = "player-server-deletion";
        public static final String NO_SERVER_FOUND = "no-server-found";
        public static final String NO_PLAYER_FOUND = "no-player-found";
        public static final String DELETE_PLAYER_CONFIRMATION_MESSAGE = "delete-player-confirmation-message";
        public static final String YES_CONFIRMATION_MESSAGE = "yes-confirmation-message";
        public static final String NO_CONFIRMATION_MESSAGE = "no-confirmation-message";
        public static final String INVALID_CONFIRMATION_MESSAGE = "invalid-confirmation-message";
        public static final String PLAYER_NOT_DELETED = "player-was-not-deleted";
        public static final String PLAYER_SERVER_ROLES_DELETED = "roles-deleted-for-player-in-server";
        public static final String PLAYER_SERVERS_ALL_ROLES_DELETED = "all-roles-deleted-for-player-in-server";
        public static final String DELETE_PLAYER_ROLES_ERROR = "delete-player-roles-error";
        public static final String DELETE_PLAYER_COLORS = "delete-player-colors";
        public static final String DELETE_PLAYER_COLORS_ERROR = "delete-player-colors-error";
        public static final String PLAYER_DELETED_FROM_SBADMINS = "player-deleted-from-sbadmins";
        public static final String PLAYER_STILL_HAVE_ROLES = "player-still-have-roles";
        public static final String DELETED_PLAYER_IS_STAFF = "deleted-player-is-staff";
        public static final String DELETE_PLAYER_ERROR = "delete-player-error";
        public static final String JOB_NOTIFY_PLAYER_REMOVAL = "job-notify-player-removal";
        public static final String JOB_NOTIFY_REMOVAL = "job-notify-removal";
        public static final String JOB_DELETING_PLAYER = "job-deleting-player";
        public static final String JOB_DELETING_PLAYERS = "job-deleting-players";
        public static final String UPDATE_EXPIRATION_DATE_QUESTION = "update-expiration-date-server-question";
        public static final String UPDATE_EXPIRATION_DATE_MONTHS_QUESTION = "update-expiration-date-months-question";
        public static final String UPDATE_EXPIRATION_DATE_SUCCESS = "update-expiration-date-success";
        public static final String UPDATE_EXPIRATION_DATE_ERROR = "update-expiration-date-error";
        public static final String DISCORD_DELETED_ROLES_FOR = "discord-deleted-roles-for";
        public static final String DISCORD_INVALID_USER = "discord-invalid-user";
        public static final String DISCORD_EMPTY_DISCORD_ID = "discord-empty-discord-id";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class CommandsConstants {

        public static final String UNKNOWN = "UNKNOWN";

        // Players
        public static final String STAFF_INFO_COMMAND =  "*- !sbm staff*";
        public static final String VIEW_PLAYER = "!sbm viewplayer";
        public static final String VIEW_PLAYER_COMMAND = getCommandWithItalics(VIEW_PLAYER);
        public static final String DELETE_PLAYER = "!sbm deleteplayer";
        public static final String DELETE_PLAYER_COMMAND = getCommandWithItalics(DELETE_PLAYER);
        public static final String RENEW_PLAYER = "!sbm renewplayer";
        public static final String RENEW_PLAYER_COMMAND = getCommandWithItalics(RENEW_PLAYER);

        public static final String ADD_ADMIN = "!sbm addadmin";
        public static final String ADD_ADMIN_COMMAND = getCommandWithItalics(ADD_ADMIN);
        public static final String UPDATE_ADMIN_COMMAND = "*- !sbm updateadmin*";

        public static final String ADD_MOD = "!sbm addmod";
        public static final String ADD_MOD_COMMAND = getCommandWithItalics(ADD_MOD);
        public static final String UPDATE_MOD_COMMAND = "*- !sbm updatemod*";

        public static final String VIPS_INFO_COMMAND = "*- !sbm vips*";
        public static final String ADD_VIP = "!sbm addvip";
        public static final String ADD_VIP_COMMAND = getCommandWithItalics(ADD_VIP);
        public static final String UPDATE_VIP_COMMAND = "*- !sbm updatevip*";

        // Bans
        public static final String BANS_INFO = "!sbm bans";
        public static final String BANS_INFO_COMMAND = getCommandWithItalics(BANS_INFO);
        public static final String ADD_BAN_COMMAND = "*- !sbm addban*";
        public static final String VIEW_BAN_COMMAND = "*- !sbm viewban*";
        public static final String UPDATE_BAN_COMMAND = "*- !sbm updateban*";
        public static final String REMOVE_BAN_COMMAND = "*- !sbm removeban*";

        public static final String COMMS_INFO =  "!sbm comms";
        public static final String COMMS_INFO_COMMAND =  getCommandWithItalics(COMMS_INFO);
        public static final String ADD_COMM_BAN_COMMAND = "*- !sbm addcommban*";
        public static final String VIEW_COMM_BAN_COMMAND = "*- !sbm viewcommban*";
        public static final String UPDATE_COMM_BAN_COMMAND = "*- !sbm updatecommban*";
        public static final String REMOVE_COMM_BAN_COMMAND = "*- !sbm removecommban*";

        private static String getCommandWithItalics(String command) {
            return String.format("*- %s*", command);
        }

        public static boolean isAddPlayerCommand(String command) {
            return ADD_VIP.equalsIgnoreCase(command) ||
                    ADD_MOD.equalsIgnoreCase(command) ||
                    ADD_ADMIN.equalsIgnoreCase(command);
        }

        public static boolean isViewPlayerCommand(String command) {
            return VIEW_PLAYER.equalsIgnoreCase(command);
        }

        public static boolean isDeletePlayerCommand(String command) {
            return DELETE_PLAYER.equalsIgnoreCase(command);
        }

        public static boolean isRenewPlayerCommand(String command) {
            return RENEW_PLAYER.equalsIgnoreCase(command);
        }
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ServerConstants {

        public static final String TURBINE = "Turbine";
        public static final String ORANGE = "Orange";
    }
}
