package net.vidyagaems.sourcebanmanager.jobs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vidyagaems.sourcebanmanager.services.QuartzJobService;
import org.jetbrains.annotations.NotNull;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleTrigger;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class NotifyRemovalJob extends QuartzJobBean {

    private final QuartzJobService quartzJobService;

    @Override
    protected void executeInternal(@NotNull JobExecutionContext context) throws JobExecutionException {
        log.info("About to run job notifyRemovalJobDetail with trigger notifyRemovalTrigger");
        quartzJobService.notifyRemovalOfOutdatedPlayers();
    }

    @Bean
    public JobDetail notifyRemovalJobDetail() {
        return JobBuilder.newJob()
                .ofType(NotifyRemovalJob.class)
                .storeDurably()
                .withIdentity("notify_removal_outdated_players")
                .withDescription("This job will notify which outdated players are going to be removed")
                .build();
    }

    @Bean
    public CronTriggerFactoryBean notifyRemovalTrigger(JobDetail notifyRemovalJobDetail) {
        CronTriggerFactoryBean cronTrigger = new CronTriggerFactoryBean();
        cronTrigger.setJobDetail(notifyRemovalJobDetail);
        cronTrigger.setName("notifyRemoval");
        // At 15:30:00pm, on every Monday and Thursday, every month
        cronTrigger.setCronExpression("0 30 15 ? * MON,WED *");
        cronTrigger.setMisfireInstruction(SimpleTrigger.REPEAT_INDEFINITELY);

        return cronTrigger;
    }
}
