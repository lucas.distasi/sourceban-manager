package net.vidyagaems.sourcebanmanager.jobs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vidyagaems.sourcebanmanager.services.QuartzJobService;
import org.jetbrains.annotations.NotNull;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SimpleTrigger;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class RemovalJob extends QuartzJobBean {

    private final QuartzJobService quartzJobService;

    @Override
    protected void executeInternal(@NotNull JobExecutionContext context) throws JobExecutionException {
        log.info("About to run job removalJobDetail with trigger removalTrigger");
        quartzJobService.removeOutdatedVips();
    }

    @Bean
    public JobDetail removalJobDetail() {
        return JobBuilder.newJob()
                .ofType(RemovalJob.class)
                .storeDurably()
                .withIdentity("remove_outdated_players")
                .withDescription("This job will remove outdated players from database. It will also remove Discord roles if Discord ID is not null or empty")
                .build();
    }

    @Bean
    public CronTriggerFactoryBean removalTrigger(JobDetail removalJobDetail) {
        CronTriggerFactoryBean cronTrigger = new CronTriggerFactoryBean();
        cronTrigger.setJobDetail(removalJobDetail);
        cronTrigger.setName("removeOutdatedPlayers");
        // At 23:55:00pm, on every Wednesday and Saturday, every month
        cronTrigger.setCronExpression("0 55 23 ? * WED,SAT *");
        cronTrigger.setMisfireInstruction(SimpleTrigger.REPEAT_INDEFINITELY);

        return cronTrigger;
    }
}
