package net.vidyagaems.sourcebanmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SourcebanManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SourcebanManagerApplication.class, args);
	}

}
