package net.vidyagaems.sourcebanmanager.repositories.sourceban;

import net.vidyagaems.sourcebanmanager.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface SourcebanPlayerRepository extends JpaRepository<Player, Integer> {

    Player findBySteamID(String steamID);
    Optional<Player> findPlayerByUsername(String username);
    Optional<Player> findPlayerBySteamID(String steamID);
    Optional<Player> findPlayerByUserID(Integer userID);

    @Transactional
    void deletePlayerBySteamID(String steamID);
}
