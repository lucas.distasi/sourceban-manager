package net.vidyagaems.sourcebanmanager.repositories.sourceban;

import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroupId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AdminServerGroupRepository extends JpaRepository<AdminServerGroup, AdminServerGroupId> {

    Optional<List<AdminServerGroup>> findByAdminIDOrderByServerID(Integer adminID);
    Optional<AdminServerGroup> findAdminServerGroupByAdminIDAndServerID(Integer adminID, Integer serverID);

    @Transactional
    void deleteAllByAdminIDAndServerID(Integer adminID, Integer serverID);

    @Query(nativeQuery = true,
            value = "SELECT * FROM sb_admins_servers_groups " +
                    "WHERE expiration_date < DATE_ADD(NOW(), INTERVAL :days DAY )")
    List<AdminServerGroup> findOutdatedRolesWithOffsetDays(Integer days);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "UPDATE sb_admins_servers_groups " +
                    "SET expiration_date = :newExpirationDate " +
                    "WHERE admin_id = :userID " +
                    "AND server_id = :serverID")
    void updatePlayerExpirationDate(LocalDate newExpirationDate, Integer userID, Integer serverID);
}
