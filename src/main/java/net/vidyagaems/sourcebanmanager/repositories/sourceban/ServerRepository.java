package net.vidyagaems.sourcebanmanager.repositories.sourceban;

import net.vidyagaems.sourcebanmanager.entities.Server;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServerRepository extends JpaRepository<Server, Integer> {

}
