package net.vidyagaems.sourcebanmanager.repositories.sourceban;

import net.vidyagaems.sourcebanmanager.entities.ServerGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServerGroupRepository extends JpaRepository<ServerGroup, Integer> {

}
