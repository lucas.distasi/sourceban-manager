package net.vidyagaems.sourcebanmanager.repositories.sourceban;

import net.vidyagaems.sourcebanmanager.entities.Ban;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourcebanBanRepository extends JpaRepository<Ban, Integer> {
}
