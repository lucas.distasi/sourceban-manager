package net.vidyagaems.sourcebanmanager.repositories.chatcolors.orange;

import net.vidyagaems.sourcebanmanager.entities.CustomChatColor;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface OrangeChatColorRepository extends JpaRepository<CustomChatColor, Integer> {

    Optional<CustomChatColor> findBySteamID(String steamID);

    @Transactional
    void deleteBySteamID(String steamID);
}
