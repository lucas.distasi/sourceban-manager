package net.vidyagaems.sourcebanmanager.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sb_admins")
public class Player {

    @Id
    @Column(name = "aid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userID;

    @Column(name = "user", nullable = false, length = 64)
    private String username;

    @Column(name = "authid", nullable = false, length = 64)
    private String steamID;

    @Column(name = "password", nullable = false, length = 128)
    private String password;

    @Column(name = "gid", nullable = false)
    private Integer groupID;

    @Column(name = "email", nullable = false, length = 128)
    private String email;

    @Column(name = "extraflags", nullable = false)
    private Integer extraFlags;

    @Column(name = "immunity", nullable = false)
    private Integer immunity;

    @Column(name = "srv_group", nullable = false)
    private String serverGroup;

    @Column(name = "srv_flags", length = 64)
    private String flags;

    @Column(name = "discordid")
    private String discordID;
}