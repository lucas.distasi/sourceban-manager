package net.vidyagaems.sourcebanmanager.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "custom_chatcolors")
public class CustomChatColor {

    @Id
    @Column(name = "[index]")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer chatColorID;

    @Column(name = "identity", nullable = false, unique = true)
    private String steamID;

    @Column(name = "tag", length = 32)
    private String tag;

    @Column(name = "tagcolor", length = 8)
    private String tagColor;

    @Column(name = "nameColor", length = 8)
    private String namecolor;

    @Column(name = "textColor", length = 8)
    private String textcolor;
}
