package net.vidyagaems.sourcebanmanager.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "sb_bans")
public class Ban {

    @Id
    @Column(name = "bid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer banID;

    @Column(name = "ip")
    private String ip;

    @Column(name = "country")
    private String country;

    @Column(name = "authid", nullable = false, length = 64)
    private String steamID;

    @Column(name = "name", nullable = false, length = 128)
    private String name;

    @Column(name = "created", nullable = false)
    private Integer banDate;

    @Column(name = "ends", nullable = false)
    private Integer banFinishDate;

    @Column(name = "length", nullable = false)
    private Integer banLength;

    @Column(name = "reason", nullable = false)
    private String reason;

    @Column(name = "aid", nullable = false)
    private Integer adminID;

    @Column(name = "RemovedBy")
    private Integer removedByID;

    @Column(name = "RemovedOn")
    private Integer removedOn;

    @Column(name = "type")
    private Integer type;

    @Column(name = "ureason")
    private String unbanReason;
}
