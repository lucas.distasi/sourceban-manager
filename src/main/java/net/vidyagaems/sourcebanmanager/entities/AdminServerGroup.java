package net.vidyagaems.sourcebanmanager.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@IdClass(AdminServerGroupId.class)
@Table(name = "sb_admins_servers_groups")
public class AdminServerGroup implements Serializable {

    @Id
    @Column(name = "admin_id", nullable = false)
    private Integer adminID;

    @Id
    @Column(name = "group_id", nullable = false)
    private Integer groupID;

    @Id
    @Column(name = "srv_group_id", nullable = false)
    private Integer serverGroupID;

    @Id
    @Column(name = "server_id", nullable = false)
    private Integer serverID;

    @Column(name = "expiration_date", nullable = false)
    private LocalDate expirationDate;
}
