package net.vidyagaems.sourcebanmanager.entities;

import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode
public class AdminServerGroupId implements Serializable {

    private Integer adminID;
    private Integer groupID;
    private Integer serverGroupID;
    private Integer serverID;
}
