package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Map;

import static net.vidyagaems.sourcebanmanager.constants.Constants.*;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.*;
import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.*;

@Service
@RequiredArgsConstructor
public class EmbedServiceImpl implements EmbedService {

    private final MessageService messageService;

    private static final String THUMBNAIL = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/836e3d27-0ea2-43b4-89f2-d901dd34de23/dby9fmj-7a815c6e-edbc-4421-a2d5-8b6daccd7af4.png/v1/fill/w_872,h_916,strp/logo_2_by_vidyagaemstf2_dby9fmj-pre.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9OTI3IiwicGF0aCI6IlwvZlwvODM2ZTNkMjctMGVhMi00M2I0LTg5ZjItZDkwMWRkMzRkZTIzXC9kYnk5Zm1qLTdhODE1YzZlLWVkYmMtNDQyMS1hMmQ1LThiNmRhY2NkN2FmNC5wbmciLCJ3aWR0aCI6Ijw9ODgzIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.bWpLbxZCnLGwJmLLnfG10iiPs-1IQE8TKA2D6zAjVDc";

    @Override
    public Map<String, MessageEmbed> getAllEmbeds() {
        return Map.of(
                BANS_PARAMETERS, getSBMBansMessageEmbed(),
                COMMS_PARAMETERS, getSBMCommsMessageEmbed(),
                STAFF_PARAMETERS, getSBMStaffMessageEmbed(),
                VIPS_PARAMETERS, getSBMVipsMessageEmbed(),
                HELP_PARAMETERS, getSBMHelpMessageEmbed()
        );
    }

    @Override
    public MessageEmbed getSBMMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Menu");
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(BANS_INFO_PROPERTY), BANS_INFO_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(COMMS_INFO_PROPERTY), COMMS_INFO_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(STAFF_INFO_PROPERTY), STAFF_INFO_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIPS_INFO_PROPERTY), VIPS_INFO_COMMAND, false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.CYAN);

        return embedBuilder.build();
    }

    @Override
    public MessageEmbed getErrorMessageEmbed(String error) {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.addField("Error", error, true);
        embedBuilder.setColor(Color.RED);

        return embedBuilder.build();
    }

    @Override
    public MessageEmbed getMessageEmbedForInexistentPlayer(String playerName) {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle(String.format(messageService.getMessage(NO_ROLE_FOR_PLAYER), playerName));
        embedBuilder.setColor(Color.ORANGE);

        return embedBuilder.build();
    }

    @Override
    public MessageEmbed getInvalidParameterEmbed(String title) {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle(title);
        embedBuilder.setColor(Color.RED);

        return embedBuilder.build();
    }

    private MessageEmbed getSBMBansMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Bans");
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(ADD_BAN_PROPERTY), ADD_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIEW_BAN_PROPERTY), VIEW_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(UPDATE_BAN_PROPERTY), UPDATE_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(REMOVE_BAN_PROPERTY), REMOVE_BAN_COMMAND, false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.GRAY);

        return embedBuilder.build();
    }

    private MessageEmbed getSBMCommsMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Comms");
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(ADD_COMM_BAN_PROPERTY), ADD_COMM_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIEW_COMM_BAN_PROPERTY), VIEW_COMM_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(UPDATE_COMM_BAN_PROPERTY), UPDATE_COMM_BAN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(REMOVE_COMM_BAN_PROPERTY), REMOVE_COMM_BAN_COMMAND, false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.BLUE);

        return embedBuilder.build();
    }

    private MessageEmbed getSBMStaffMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Staff");
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(ADD_ADMIN_PROPERTY), ADD_ADMIN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIEW_ADMIN_PROPERTY), VIEW_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(UPDATE_ADMIN_PROPERTY), UPDATE_ADMIN_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(REMOVE_ADMIN_PROPERTY), DELETE_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(RENEW_ADMIN_PROPERTY), RENEW_PLAYER_COMMAND, false);
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(ADD_MOD_PROPERTY), ADD_MOD_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIEW_MOD_PROPERTY), VIEW_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(UPDATE_MOD_PROPERTY), UPDATE_MOD_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(REMOVE_MOD_PROPERTY), DELETE_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(RENEW_MOD_PROPERTY), RENEW_PLAYER_COMMAND, false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.YELLOW);

        return embedBuilder.build();
    }

    private MessageEmbed getSBMVipsMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Vips");
        embedBuilder.addBlankField(true);
        embedBuilder.addField(messageService.getMessage(ADD_VIP_PROPERTY), ADD_VIP_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(VIEW_VIP_PROPERTY), VIEW_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(UPDATE_VIP_PROPERTY), UPDATE_VIP_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(REMOVE_VIP_PROPERTY), DELETE_PLAYER_COMMAND, false);
        embedBuilder.addField(messageService.getMessage(RENEW_VIP_PROPERTY), RENEW_PLAYER_COMMAND, false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.GREEN);

        return embedBuilder.build();
    }

    private MessageEmbed getSBMHelpMessageEmbed() {
        var embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Sourceban Manager - Help");
        embedBuilder.addBlankField(true);
        embedBuilder.addField("", messageService.getMessage(COMMANDS_TITLE_PROPERTY), true);
        embedBuilder.addField(SBM_COMMAND, messageService.getMessage(SBM_COMMANDS_HELP), false);
        embedBuilder.addBlankField(true);
        embedBuilder.addField("", messageService.getMessage(PARAMETERS_TITLE_PROPERTY), true);
        embedBuilder.addField(messageService.getMessage(STEAMID_PARAMETER), messageService.getMessage(STEAMID_PARAMETER_DESCRIPTION), false);
        embedBuilder.addField(messageService.getMessage(SERVER_PARAMETER), messageService.getMessage(SERVER_PARAMETER_DESCRIPTION), false);
        embedBuilder.addField(messageService.getMessage(TAGNAME_PARAMETER), messageService.getMessage(TAGNAME_PARAMETER_DESCRIPTION), false);
        embedBuilder.addField(messageService.getMessage(TAGCOLOR_PARAMETER), messageService.getMessage(TAGCOLOR_PARAMETER_DESCRIPTION), false);
        embedBuilder.addField(messageService.getMessage(DAYS_PARAMETER), messageService.getMessage(DAYS_PARAMETER_DESCRIPTION), false);
        embedBuilder.setThumbnail(THUMBNAIL);
        embedBuilder.setColor(Color.BLACK);

        return embedBuilder.build();
    }
}
