package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageSource messageSource;

    public String getMessage(String property) {
        return messageSource.getMessage(property, null, LocaleContextHolder.getLocale());
    }
}
