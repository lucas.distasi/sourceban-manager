package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.CustomChatColor;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.repositories.chatcolors.orange.OrangeChatColorRepository;
import net.vidyagaems.sourcebanmanager.repositories.chatcolors.turbine.TurbineChatColorRepository;
import net.vidyagaems.sourcebanmanager.services.ChatColorsService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import org.owasp.security.logging.SecurityMarkers;
import org.springframework.stereotype.Service;

import java.awt.Color;
import java.util.Optional;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_CHAT_COLORS_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_CHAT_COLORS_SUCCESSFULLY;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DELETE_PLAYER_COLORS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DELETE_PLAYER_COLORS_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_CHATCOLOR_EXISTS_WARNING;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ServerConstants.ORANGE;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ServerConstants.TURBINE;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatColorsServiceImpl implements ChatColorsService {

    private final ServerService serverService;
    private final MessageService messageService;
    private final TurbineChatColorRepository turbineChatColorRepository;
    private final OrangeChatColorRepository orangeChatColorRepository;

    @Override
    public MessageEmbed saveChatColor(PlayerModel playerModel) {
        var messageEmbed = new EmbedBuilder();
        var tagName = String.format("[%s] ", playerModel.getTagName());

        var chatColor = new CustomChatColor();
        chatColor.setSteamID(playerModel.getSteamID());
        chatColor.setTag(tagName);
        chatColor.setTagColor(playerModel.getTagColor());
        chatColor.setNamecolor(null);
        chatColor.setTextcolor(null);

        try {
            var serverID = playerModel.getRoleServer().getServer();
            var server = serverService.findServerNameByServerIDOrServerName(String.valueOf(serverID)).orElse(UNKNOWN);
            saveColor(server, chatColor, messageEmbed);
        } catch (Exception ex) {
            log.error(SecurityMarkers.EVENT_FAILURE, "An error occurred when saving color {} for Player {}. Error: {}",
                    chatColor, playerModel, ex.getMessage());
            messageEmbed.addField(messageService.getMessage(ADD_PLAYER_CHAT_COLORS_ERROR), "", false);
            messageEmbed.setColor(Color.RED);
        }

        return messageEmbed.build();
    }

    @Override
    public Optional<CustomChatColor> findTurbineChatColorBySteamID(String steamID) {
        return turbineChatColorRepository.findBySteamID(steamID);
    }

    @Override
    public Optional<CustomChatColor> findOrangeChatColorBySteamID(String steamID) {
        return orangeChatColorRepository.findBySteamID(steamID);
    }

    @Override
    public MessageEmbed deletePlayerColors(String steamID, String server) {
        var serverName = serverService.findServerNameByServerIDOrServerName(server).orElse(UNKNOWN);
        var embedBuilder = new EmbedBuilder();
        String title;

        try {
            if (ORANGE.equalsIgnoreCase(serverName)) {
                orangeChatColorRepository.deleteBySteamID(steamID);
            }

            if (TURBINE.equalsIgnoreCase(serverName)) {
                turbineChatColorRepository.deleteBySteamID(steamID);
            }

            title = String.format(messageService.getMessage(DELETE_PLAYER_COLORS), steamID, serverName);
            embedBuilder.setColor(Color.GREEN);
        } catch (Exception ex) {
            log.error(SecurityMarkers.EVENT_FAILURE, "An error has occurred when trying to remove chat colors for Steam ID {} in server {}. Error {}",
                    steamID, serverName, ex.getMessage());
            title = String.format(messageService.getMessage(DELETE_PLAYER_COLORS_ERROR), steamID, server);
            embedBuilder.setColor(Color.RED);
        }

        embedBuilder.setTitle(title);
        return embedBuilder.build();
    }

    private void saveColor(String server, CustomChatColor chatColor, EmbedBuilder messageEmbed) {
        if (!TURBINE.equalsIgnoreCase(server) && !ORANGE.equalsIgnoreCase(server)) {
            log.warn("Server is unknown");
            messageEmbed.setTitle(UNKNOWN);
            messageEmbed.addField("Server is unknown", "", false);
            messageEmbed.setColor(Color.ORANGE);
        }

        if (TURBINE.equalsIgnoreCase(server)) {
            saveTurbineColor(chatColor, messageEmbed);
        }

        if (ORANGE.equalsIgnoreCase(server)) {
            saveOrangeColor(chatColor, messageEmbed);
        }
    }

    private void saveTurbineColor(CustomChatColor chatColor, EmbedBuilder messageEmbed) {
        var color = turbineChatColorRepository.findBySteamID(chatColor.getSteamID());

        color.ifPresentOrElse(playerChatColor -> {
            sendWarningMessageIfColorExists(TURBINE, messageEmbed);
            log.warn("Player {} already have colors in {}", playerChatColor.getSteamID(), TURBINE);
        }, () -> {
            turbineChatColorRepository.save(chatColor);
            messageEmbed.setTitle(TURBINE);
            messageEmbed.addField(messageService.getMessage(ADD_PLAYER_CHAT_COLORS_SUCCESSFULLY), "", false);
            messageEmbed.setColor(Color.GREEN);
            log.info("Saving color {} in server {}", chatColor, TURBINE);
        });
    }

    private void saveOrangeColor(CustomChatColor chatColor, EmbedBuilder messageEmbed) {
        var color = orangeChatColorRepository.findBySteamID(chatColor.getSteamID());

        color.ifPresentOrElse(playerChatColor -> {
            sendWarningMessageIfColorExists(ORANGE, messageEmbed);
            log.warn("Player {} already have colors in {}", playerChatColor.getSteamID(), ORANGE);
        }, () -> {
            orangeChatColorRepository.save(chatColor);
            messageEmbed.setTitle(ORANGE);
            messageEmbed.addField(messageService.getMessage(ADD_PLAYER_CHAT_COLORS_SUCCESSFULLY), "", false);
            messageEmbed.setColor(Color.GREEN);
            log.info("Saving color {} in server {}", chatColor, ORANGE);
        });
    }

    private void sendWarningMessageIfColorExists(String server, EmbedBuilder messageEmbed) {
        messageEmbed.setTitle(server);
        messageEmbed.addField(messageService.getMessage(PLAYER_CHATCOLOR_EXISTS_WARNING), "", false);
        messageEmbed.setColor(Color.ORANGE);
    }
}
