package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.vidyagaems.sourcebanmanager.services.JDAService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class JDAServiceImpl implements JDAService {

    @Value("${discord.server-id}")
    private Long guildID;

    private final JDA getJda;

    public JDAServiceImpl(@Lazy JDA getJda) {
        this.getJda = getJda;
    }

    @Override
    public boolean isUserInServer(String discordID) {
        var guild = Optional.ofNullable(getJda.getGuildById(guildID));

        if (guild.isPresent()) {
            log.info("Checking if Discord ID {} is in server", discordID);
            var server = guild.get();
            var user = Optional.ofNullable(server.getMemberById(discordID));

            return user.isPresent();
        } else {
            log.error("Guild ID {} does not exists.", guildID);
            return false;
        }
    }
}
