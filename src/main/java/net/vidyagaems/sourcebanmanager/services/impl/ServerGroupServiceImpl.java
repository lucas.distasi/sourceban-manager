package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.ServerGroup;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.ServerGroupRepository;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.ServerGroupService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.owasp.security.logging.SecurityMarkers;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;
import java.util.Optional;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_SERVER_GROUP_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_SERVER_GROUP_SUCCESSFULLY;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DELETE_PLAYER_ROLES_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_PLAYER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_SERVER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_ALREADY_HAS_ROLE_IN_SERVER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_SERVER_ROLES_DELETED;

@Slf4j
@Service
@RequiredArgsConstructor
public class ServerGroupServiceImpl implements ServerGroupService {

    private final ServerService serverService;
    private final MessageService messageService;
    private final ServerGroupRepository serverGroupRepository;
    private final SourceBanPlayerService sourcebanPlayerService;
    private final AdminServerGroupService adminServerGroupService;

    @Override
    public MessageEmbed saveRole(PlayerModel playerModel) {
        var messageEmbed = new EmbedBuilder();

        try {
            var savedPlayer = sourcebanPlayerService.findBySteamID(playerModel.getSteamID());
            var roleID = getServerGroupID(playerModel.getRoleServer().getRole());
            var roleServer = getServerGroupName(roleID);
            var playerID = savedPlayer.getUserID();
            var adminServerGroup = getAdminServerGroup(playerModel, roleServer, playerID);
            var currentPlayerPrivileges = adminServerGroupService
                    .findAdminServerGroupByAdminIDAndServerID(adminServerGroup.getAdminID(), adminServerGroup.getServerID());

            currentPlayerPrivileges.ifPresentOrElse(serverGroup -> {
                var currentRole = currentPlayerPrivileges.map(group -> getServerGroupName(group.getGroupID())).orElse(UNKNOWN);
                var message = String.format(messageService.getMessage(PLAYER_ALREADY_HAS_ROLE_IN_SERVER),
                        currentRole.toUpperCase());

                messageEmbed.addField(message, "", false);
                messageEmbed.setColor(Color.ORANGE);
                log.warn("Player {} already has role {}", playerModel.getSteamID(), currentRole);
            }, () -> {
                adminServerGroupService.save(adminServerGroup);
                messageEmbed.addField(messageService.getMessage(ADD_PLAYER_SERVER_GROUP_SUCCESSFULLY), "", false);
                messageEmbed.setColor(Color.GREEN);
                log.info(SecurityMarkers.EVENT_SUCCESS, "Saved role for player {} with Server Group {}", savedPlayer, adminServerGroup);
            });
        } catch (Exception ex) {
            log.error(SecurityMarkers.EVENT_FAILURE, "An error occurred saving role for player {}. Error: {}", playerModel, ex.getMessage());
            messageEmbed.addField(messageService.getMessage(ADD_PLAYER_SERVER_GROUP_ERROR), "", false);
            messageEmbed.setColor(Color.RED);
        }

        return messageEmbed.build();
    }

    @Override
    public MessageEmbed deletePlayerRoles(String steamID, String server) {
        var embedBuilder = new EmbedBuilder();
        var serverID = serverService.findServerIDByServerNameOrServerID(server);
        var adminID = Optional.ofNullable(sourcebanPlayerService.findBySteamID(steamID).getUserID());
        String title;

        try {
            if (adminID.isPresent() && serverID.isPresent()) {
                adminServerGroupService.deleteAllByAdminIDAndServerID(adminID.get(), serverID.get());
                log.info(SecurityMarkers.EVENT_SUCCESS, "Removed rol for player with Steam ID {} in server {}", steamID, server);
                title = String.format(messageService.getMessage(PLAYER_SERVER_ROLES_DELETED), steamID, server);
                embedBuilder.setColor(Color.GREEN);
            } else {
                var message = adminID.isEmpty() ?
                        String.format(messageService.getMessage(NO_PLAYER_FOUND), adminID) :
                        String.format(messageService.getMessage(NO_SERVER_FOUND), serverID);
                log.warn(message);
                title = message;
                embedBuilder.setColor(Color.ORANGE);
            }
        } catch (Exception ex) {
            log.error(SecurityMarkers.EVENT_FAILURE, "An error has occurred when trying to remove roles for Steam ID {} in server {}. Error {}",
                    steamID, server, ex.getMessage());
            title = String.format(messageService.getMessage(DELETE_PLAYER_ROLES_ERROR), steamID, server);
            embedBuilder.setColor(Color.RED);
        }

        embedBuilder.setTitle(title);
        return embedBuilder.build();
    }

    @Override
    @Cacheable(value = "findAllServerGroups", sync = true)
    public List<ServerGroup> findAllServerGroups() {
        log.info("Retrieving all server groups");
        return serverGroupRepository.findAll();
    }

    @Override
    @Cacheable(value = "getServerGroupName", key = "#serverGroupID")
    public String getServerGroupName(Integer serverGroupID) {
        log.info("Retrieving server group name with id {}", serverGroupID);
        return findAllServerGroups().stream()
                .filter(serverGroup -> serverGroupID.equals(serverGroup.getServerGroupID()))
                .map(ServerGroup::getRoleName)
                .findFirst()
                .orElse(UNKNOWN)
                .toUpperCase();
    }

    @Override
    @Cacheable(value = "getServerGroupID", key = "#serverGroupName")
    public Integer getServerGroupID(String serverGroupName) {
        log.info("Retrieving server group id for server group name {}", serverGroupName);
        return findAllServerGroups().stream()
                .filter(serverGroup -> serverGroupName.equalsIgnoreCase(serverGroup.getRoleName()))
                .map(ServerGroup::getServerGroupID)
                .findFirst()
                .orElse(-1);
    }

    private AdminServerGroup getAdminServerGroup(PlayerModel playerModel, String roleServer, Integer playerID) {
        var groupID = getServerGroup(roleServer);

        var adminServerGroup = new AdminServerGroup();
        groupID.ifPresentOrElse(group -> {
            var serverID = playerModel.getRoleServer().getServer();
            var serverGroupID = -1;

            adminServerGroup.setAdminID(playerID);
            adminServerGroup.setGroupID(group.getServerGroupID());
            adminServerGroup.setServerID(serverID);
            adminServerGroup.setServerGroupID(serverGroupID);
            adminServerGroup.setExpirationDate(playerModel.getExpirationDate());
        }, () -> log.error(SecurityMarkers.EVENT_FAILURE, "Server role {} does not exist", roleServer));

        return adminServerGroup;
    }

    @Override
    @Cacheable(value = "getServerGroup", key = "#roleServer")
    public Optional<ServerGroup> getServerGroup(String roleServer) {
        log.info("Retrieving Admin Server Group for {}", roleServer);
        return findAllServerGroups()
                .stream()
                .filter(serverGroup -> serverGroup.getRoleName().equalsIgnoreCase(roleServer))
                .findFirst();
    }
}
