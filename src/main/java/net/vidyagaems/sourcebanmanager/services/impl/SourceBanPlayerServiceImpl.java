package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.Player;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.SourcebanPlayerRepository;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import net.vidyagaems.sourcebanmanager.services.mappings.EntityMapperService;
import org.apache.commons.collections4.CollectionUtils;
import org.owasp.security.logging.SecurityMarkers;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Optional;
import java.util.UUID;

import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.ADD_PLAYER_SUCCESSFULLY;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DELETE_PLAYER_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_PLAYER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_ROLE_FOR_PLAYER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_SERVER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_DELETED_FROM_SBADMINS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_EXISTS_WARNING;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_STILL_HAVE_ROLES;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.UPDATE_EXPIRATION_DATE_ERROR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.UPDATE_EXPIRATION_DATE_SUCCESS;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidSteamID;

@Slf4j
@Service
@RequiredArgsConstructor
public class SourceBanPlayerServiceImpl implements SourceBanPlayerService {

    private final MessageService messageService;
    private final AdminServerGroupService adminServerGroupService;
    private final SourcebanPlayerRepository sourcebanPlayerRepository;
    private final EntityMapperService<Player, PlayerModel> entityMapperService;

    @Override
    public MessageEmbed savePlayer(PlayerModel playerModel) {
        var player = entityMapperService.mapFrom(playerModel);
        var messageEmbed = new EmbedBuilder();
        var playerExists = findBySteamID(playerModel.getSteamID());

        if (playerExists != null) {
            messageEmbed.addField(messageService.getMessage(PLAYER_EXISTS_WARNING), "", false);
            messageEmbed.setColor(Color.ORANGE);
            log.warn("Player {} already exists in sb_admins", playerModel);
        } else {
            try {
                log.info("Saving player {}", player);
                sourcebanPlayerRepository.save(player);
                messageEmbed.addField(messageService.getMessage(ADD_PLAYER_SUCCESSFULLY), "", false);
                messageEmbed.setColor(Color.GREEN);
                log.info(SecurityMarkers.EVENT_SUCCESS, "Saved player {} into sb_admins", player);
            } catch (Exception ex) {
                log.error(SecurityMarkers.EVENT_FAILURE, "An error occurred when saving player {}. Error: {}", player, ex.getMessage());
                messageEmbed.addField(messageService.getMessage(ADD_PLAYER_ERROR), "", false);
                messageEmbed.setColor(Color.RED);
            }
        }

        return messageEmbed.build();
    }

    @Override
    public MessageEmbed deletePlayer(String steamID) {
        var embedBuilder = new EmbedBuilder();
        var player = findBySteamID(steamID);
        var adminID = Optional.ofNullable(player.getUserID());
        String title;

        try {
            if (adminID.isPresent()) {
                var serverGroups = adminServerGroupService.findPlayerServerGroups(adminID.get());

                if (serverGroups.isEmpty() || CollectionUtils.isEmpty(serverGroups.get())) {
                    log.info("Deleting player with Steam ID {} from sb_admins", steamID);
                    sourcebanPlayerRepository.deletePlayerBySteamID(steamID);

                    title = String.format(messageService.getMessage(PLAYER_DELETED_FROM_SBADMINS), steamID);
                    embedBuilder.setColor(Color.GREEN);
                } else {
                    title = String.format(messageService.getMessage(PLAYER_STILL_HAVE_ROLES), steamID);
                    embedBuilder.setColor(Color.ORANGE);
                }
            } else {
                title = String.format(messageService.getMessage(NO_ROLE_FOR_PLAYER), steamID);
                log.warn("No roles found for player with Steam ID {}", steamID);
                embedBuilder.setColor(Color.ORANGE);
            }
        } catch (Exception ex) {
            title = String.format(messageService.getMessage(DELETE_PLAYER_ERROR), steamID);
            log.error(SecurityMarkers.EVENT_FAILURE, "An error has occurred when trying to remove player with Steam ID {} from sb_admins. Error {}",
                    steamID, ex.getMessage());
            embedBuilder.setColor(Color.RED);
        }

        embedBuilder.setTitle(title);
        return embedBuilder.build();
    }

    @Override
    public MessageEmbed renewPlayerExpirationDate(int monthsToAdd, String steamID, Integer serverID) {
        var embedBuilder = new EmbedBuilder();
        var player = Optional.ofNullable(findBySteamID(steamID));
        String title;

        if (serverID.equals(0)) {
            embedBuilder.setTitle(String.format(messageService.getMessage(NO_SERVER_FOUND), serverID));
            embedBuilder.setColor(Color.RED);

            return embedBuilder.build();
        }

        try {
            if (player.isPresent()) {
                var userID = player.get().getUserID();
                adminServerGroupService.updatePlayerExpirationDate(monthsToAdd, userID, serverID);

                title = String.format(messageService.getMessage(UPDATE_EXPIRATION_DATE_SUCCESS), steamID);
                embedBuilder.setColor(Color.GREEN);
            } else {
                title = String.format(messageService.getMessage(NO_PLAYER_FOUND), steamID);
                embedBuilder.setColor(Color.ORANGE);

                log.warn("Player with Steam ID {} was not found", steamID);
            }
        } catch (Exception ex) {
            title = String.format(messageService.getMessage(UPDATE_EXPIRATION_DATE_ERROR), steamID);
            log.error(ex.getMessage());
            embedBuilder.setColor(Color.RED);
        }

        embedBuilder.setTitle(title);
        return embedBuilder.build();
    }

    @Override
    public Player findBySteamID(String steamID) {
        log.info("Retrieving info for user with Steam ID {}", steamID);
        return sourcebanPlayerRepository.findBySteamID(steamID);
    }

    @Override
    public String findSteamIDFromParam(String parameter) {
        log.info("Retrieving Steam ID for player with parameter {}", parameter);
        return isValidSteamID(parameter) ?
                parameter :
                sourcebanPlayerRepository
                        .findPlayerByUsername(parameter)
                        .map(Player::getSteamID)
                        .orElse(parameter);
    }

    @Override
    public Optional<Player> findPlayerByUsername(String username) {
        log.info("Searching if player with username {} exists", username);
        return sourcebanPlayerRepository.findPlayerByUsername(username);
    }

    @Override
    public Optional<Player> findPlayerByUserID(Integer userID) {
        log.info("Retrieving player with userid {}", userID);
        return sourcebanPlayerRepository.findPlayerByUserID(userID);
    }

    @Override
    public Optional<Player> findPlayerBySteamID(String steamID) {
        log.info("Retrieving info for player with Steam ID {}", steamID);
        return sourcebanPlayerRepository.findPlayerBySteamID(steamID);
    }
}
