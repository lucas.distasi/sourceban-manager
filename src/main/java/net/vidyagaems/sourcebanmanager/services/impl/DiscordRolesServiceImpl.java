package net.vidyagaems.sourcebanmanager.services.impl;

import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscordRolesServiceImpl implements DiscordRolesService {

    @Value("${discord.role.staff}")
    private String discordStaffRoleID;

    @Override
    public boolean hasStaffRole(List<String> roles) {
        return roles.contains(discordStaffRoleID);
    }
}
