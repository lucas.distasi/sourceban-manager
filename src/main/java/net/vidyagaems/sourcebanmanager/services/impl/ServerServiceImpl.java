package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.ServerRepository;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;

@Slf4j
@Service
@RequiredArgsConstructor
public class ServerServiceImpl implements ServerService {

    private final ServerRepository serverRepository;
    private final AdminServerGroupService adminServerGroupService;

    @Override
    @Cacheable(value = "findAllServers")
    public List<Server> findAll() {
        log.info("Searching all servers");
        return serverRepository.findAll();
    }

    @Override
    @Cacheable(value = "findServerNameByServerIDOrServerName", key = "#param")
    public Optional<String> findServerNameByServerIDOrServerName(String param) {
        log.info("Retrieving server name for server {}", param);

        return findAll().stream()
                .filter(server -> param.equalsIgnoreCase(server.getServerName()) ||
                        param.equalsIgnoreCase(String.valueOf(server.getServerID())))
                .findFirst()
                .map(Server::getServerName);
    }

    @Override
    @Cacheable(value = "findServerIDByServerNameOrServerID", key = "#param")
    public Optional<Integer> findServerIDByServerNameOrServerID(String param) {
        log.info("Retrieving server id for server {}", param);

        return findAll().stream()
                .filter(server -> param.equalsIgnoreCase(server.getServerName()) ||
                        param.equalsIgnoreCase(String.valueOf(server.getServerID())))
                .findFirst()
                .map(Server::getServerID);
    }

    @Override
    public List<Server> findServersForAdminID(Integer adminID) {
        return adminServerGroupService.findPlayerServerGroups(adminID)
                .map(this::getServerID)
                .orElse(Collections.emptyList());
    }

    private List<Server> getServerID(List<AdminServerGroup> adminServerGroup) {
        return adminServerGroup.stream()
                .map(serverGroup -> {
                    var server = new Server();
                    server.setServerID(serverGroup.getServerID());
                    server.setServerName(findServerNameByServerIDOrServerName(String.valueOf(serverGroup.getServerID())).orElse(UNKNOWN));

                    return server;
                })
                .collect(Collectors.toList());
    }
}
