package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.CustomChatColor;
import net.vidyagaems.sourcebanmanager.entities.ServerGroup;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import net.vidyagaems.sourcebanmanager.services.ChatColorsService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import net.vidyagaems.sourcebanmanager.services.ServerGroupService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_INFORMATION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ROOT;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ServerConstants.ORANGE;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ServerConstants.TURBINE;
import static net.vidyagaems.sourcebanmanager.utils.DateUtils.DD_MM_YYYY;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlayerServiceImpl implements PlayerService {

    private final EmbedService embedService;
    private final ServerService serverService;
    private final MessageService messageService;
    private final ChatColorsService chatColorsService;
    private final ServerGroupService serverGroupService;
    private final SourceBanPlayerService sourcebanPlayerService;
    private final AdminServerGroupService adminServerGroupService;

    @Override
    public List<MessageEmbed> savePlayerWithRoleAndChatColor(PlayerModel playerModel) {
        return Arrays.asList(
                sourcebanPlayerService.savePlayer(playerModel),
                serverGroupService.saveRole(playerModel),
                chatColorsService.saveChatColor(playerModel)
        );
    }

    @Override
    public MessageEmbed getPlayerInformation(String param) {
        var steamID = sourcebanPlayerService.findSteamIDFromParam(param);
        var playerInfo = sourcebanPlayerService.findPlayerBySteamID(steamID);

        if (playerInfo.isPresent()) {
            var embedBuilder = new EmbedBuilder();
            var player = playerInfo.get();
            var playerRoles = adminServerGroupService.findPlayerServerGroups(player.getUserID());
            var turbinePlayerChatColor = chatColorsService.findTurbineChatColorBySteamID(player.getSteamID());
            var orangePlayerChatColor = chatColorsService.findOrangeChatColorBySteamID(player.getSteamID());

            embedBuilder.setTitle(String.format(messageService.getMessage(PLAYER_INFORMATION), player.getUsername()));
            embedBuilder.addField("Steam ID", player.getSteamID(), false);
            Optional.ofNullable(player.getDiscordID()).ifPresent(discordId -> {
                var userDiscordID = String.format("<@%s>", discordId);
                embedBuilder.addField("Discord", userDiscordID, false);
            });
            embedBuilder.addBlankField(false);
            embedBuilder.setColor(Color.PINK);
            playerRoles.ifPresent(serverGroups -> addPlayerRolesToEmbed(serverGroups, embedBuilder));
            turbinePlayerChatColor.ifPresent(customChatColor -> addTurbineChatColorsToEmbed(customChatColor, embedBuilder));
            orangePlayerChatColor.ifPresent(customChatColor -> addOrangeChatColorsToEmbed(customChatColor, embedBuilder));

            return embedBuilder.build();
        } else {
            log.warn("No info found for player with Steam ID {}", steamID);
            return embedService.getMessageEmbedForInexistentPlayer(steamID);
        }
    }

    @Override
    public List<MessageEmbed> deletePlayer(String steamID, String server) {
        return Arrays.asList(
                chatColorsService.deletePlayerColors(steamID, server),
                serverGroupService.deletePlayerRoles(steamID, server),
                sourcebanPlayerService.deletePlayer(steamID)
        );
    }

    @Override
    public boolean nonRootPlayerExistsAndHasRoles(String parameter) {
        var steamID = sourcebanPlayerService.findSteamIDFromParam(parameter);
        var player = sourcebanPlayerService.findPlayerBySteamID(steamID);

        if (player.isEmpty()) {
            return false;
        }

        var playerRoles = adminServerGroupService.findPlayerServerGroups(player.get().getUserID());
        var rootRole = serverGroupService.findAllServerGroups()
                .stream()
                .filter(role -> ROOT.equalsIgnoreCase(role.getRoleName()))
                .findFirst();

        if (playerRoles.isEmpty() || rootRole.isEmpty() || CollectionUtils.isEmpty(playerRoles.get())) {
            return false;
        }

        return isNotRoot(playerRoles.get(), rootRole.get());
    }

    private boolean isNotRoot(List<AdminServerGroup> playerRoles, ServerGroup rootRole) {
        return playerRoles
                .stream()
                .noneMatch(group -> rootRole.getServerGroupID().equals(group.getGroupID()));
    }

    private void addPlayerRolesToEmbed(List<AdminServerGroup> adminServerGroups, EmbedBuilder embedBuilder) {
        adminServerGroups.forEach(adminServerGroup -> {
            var formattedDate = adminServerGroup.getExpirationDate().format(DD_MM_YYYY);

            embedBuilder.addField("Role", getRole(adminServerGroup.getGroupID()), true);
            embedBuilder.addField("Server", getServer(adminServerGroup.getServerID()), true);
            embedBuilder.addField("Expiration Date", formattedDate, true);
            embedBuilder.addBlankField(false);
        });
    }

    private void addTurbineChatColorsToEmbed(CustomChatColor customChatColor, EmbedBuilder embedBuilder) {
        embedBuilder.addField(TURBINE, "", false);
        addGenericColorFields(customChatColor, embedBuilder);
        embedBuilder.addBlankField(false);
    }

    private void addOrangeChatColorsToEmbed(CustomChatColor customChatColor, EmbedBuilder embedBuilder) {
        embedBuilder.addField(ORANGE, "", false);
        addGenericColorFields(customChatColor, embedBuilder);
    }

    private void addGenericColorFields(CustomChatColor customChatColor, EmbedBuilder embedBuilder) {
        embedBuilder.addField("Tag", customChatColor.getTag(), true);
        embedBuilder.addField("Tag Color", String.format("#%s", customChatColor.getTagColor().toUpperCase()), true);

        if (!StringUtils.isBlank(customChatColor.getNamecolor())) {
            embedBuilder.addField("Name Color", String.format("#%s", customChatColor.getNamecolor()), true);
        }
    }

    private String getRole(Integer groupID) {
        return serverGroupService.findAllServerGroups()
                .stream()
                .filter(serverGroup -> serverGroup.getServerGroupID().equals(groupID))
                .findFirst()
                .map(ServerGroup::getRoleName)
                .orElse(UNKNOWN)
                .toUpperCase();
    }

    private String getServer(Integer serverID) {
        return serverService
                .findServerNameByServerIDOrServerName(String.valueOf(serverID))
                .orElse(UNKNOWN);
    }
}
