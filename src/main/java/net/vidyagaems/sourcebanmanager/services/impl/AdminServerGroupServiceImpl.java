package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.AdminServerGroupRepository;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminServerGroupServiceImpl implements AdminServerGroupService {

    private final AdminServerGroupRepository adminServerGroupRepository;

    @Override
    public void save(AdminServerGroup adminServerGroup) {
        log.info("Saving Admin Server Group {}", adminServerGroup.toString());
        adminServerGroupRepository.save(adminServerGroup);
    }

    @Override
    public Optional<List<AdminServerGroup>> findPlayerServerGroups(Integer adminID) {
        log.info("Searching server groups for Admin ID {}", adminID);
        return adminServerGroupRepository.findByAdminIDOrderByServerID(adminID);
    }

    @Override
    public Optional<AdminServerGroup> findAdminServerGroupByAdminIDAndServerID(Integer adminID, Integer serverID) {
        log.info("Retrieving info for player with Admin ID {} and Server ID {}", adminID, serverID);
        return adminServerGroupRepository.findAdminServerGroupByAdminIDAndServerID(adminID, serverID);
    }

    @Override
    public void deleteAllByAdminIDAndServerID(Integer adminID, Integer serverID) {
        log.info("Removing roles for Admin ID {} in Server ID {}", adminID, serverID);
        adminServerGroupRepository.deleteAllByAdminIDAndServerID(adminID, serverID);
    }

    @Override
    @Cacheable(value = "findOutdatedPlayersRolesWithOffsetDays", key = "#days")
    public List<AdminServerGroup> findOutdatedPlayersRolesWithOffsetDays(int days) {
        log.info("Searching for outdated players with offset of {} days", days);

        return Optional.of(adminServerGroupRepository.findOutdatedRolesWithOffsetDays(days))
                .orElse(Collections.emptyList());
    }

    @Override
    public void updatePlayerExpirationDate(int monthsToAdd, int adminID, Integer serverID) {
        var adminServerGroup = adminServerGroupRepository.findAdminServerGroupByAdminIDAndServerID(adminID, serverID);

        if (adminServerGroup.isPresent()) {
            var expirationDate = adminServerGroup.get().getExpirationDate();
            var newExpirationDate = expirationDate.plusMonths(monthsToAdd);

            adminServerGroupRepository.updatePlayerExpirationDate(newExpirationDate, adminID, serverID);
            log.info("Updated expiration date for User ID {} in Server ID {}. New expiration date: {}",
                    adminID, serverID, newExpirationDate);
        }
    }
}
