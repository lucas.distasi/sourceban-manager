package net.vidyagaems.sourcebanmanager.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.Player;
import net.vidyagaems.sourcebanmanager.services.AdminServerGroupService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import net.vidyagaems.sourcebanmanager.services.QuartzJobService;
import net.vidyagaems.sourcebanmanager.services.ServerGroupService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static net.vidyagaems.sourcebanmanager.constants.Constants.ADMIN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.MOD;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DISCORD_DELETED_ROLES_FOR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DISCORD_EMPTY_DISCORD_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.DISCORD_INVALID_USER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.JOB_DELETING_PLAYER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.JOB_DELETING_PLAYERS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.JOB_NOTIFY_PLAYER_REMOVAL;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.JOB_NOTIFY_REMOVAL;
import static net.vidyagaems.sourcebanmanager.constants.Constants.VIP;
import static net.vidyagaems.sourcebanmanager.utils.DateUtils.DD_MM_YYYY;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuartzJobServiceImpl implements QuartzJobService {

    private final JDA getJda;
    private final ServerService serverService;
    private final PlayerService playerService;
    private final MessageService messageService;
    private final ServerGroupService serverGroupService;
    private final SourceBanPlayerService sourcebanPlayerService;
    private final AdminServerGroupService adminServerGroupService;

    @Value("${discord.role.staff}")
    private String staffRoleID;

    @Value("${discord.channel.scheduler}")
    private String schedulerChannelID;

    @Value("${discord.channel.vip-reminder}")
    private String vipRemindersChannelID;

    @Value("${discord.channel.staff-reminder}")
    private String staffRemindersChannelID;

    @Value("${discord.role.vip-staff}")
    private List<Long> vipAndStaffRoles;

    @Value("${discord.server-id}")
    private Long guildID;

    @Override
    public void notifyRemovalOfOutdatedPlayers() {
        var schedulerChannel = Optional.ofNullable(getJda.getTextChannelById(schedulerChannelID));
        var vipReminderChannel = Optional.ofNullable(getJda.getTextChannelById(vipRemindersChannelID));
        var staffReminderChannel = Optional.ofNullable(getJda.getTextChannelById(staffRemindersChannelID));
        var outdatedPlayers = adminServerGroupService.findOutdatedPlayersRolesWithOffsetDays(2);
        var outdatedVipPlayers = getOutdatedVipPlayers(outdatedPlayers);
        var outdatedStaffPlayers = getOutdatedStaffPlayers(outdatedPlayers);

        schedulerChannel.ifPresentOrElse(this::sendMessageToSchedulerChannel,
                () -> log.error("Scheduler channel with ID {} does not exist", schedulerChannelID));

        vipReminderChannel.ifPresentOrElse(textChannel -> sendMessageToReminderChannel(textChannel, outdatedVipPlayers),
                () -> log.error("Vip reminder channel with ID {} does not exist", vipRemindersChannelID));

        staffReminderChannel.ifPresentOrElse(textChannel -> sendMessageToReminderChannel(textChannel, outdatedStaffPlayers),
                () -> log.error("Staff reminder channel with ID {} does not exist", staffRemindersChannelID));
    }

    @Override
    public void removeOutdatedVips() {
        var schedulerChannel = Optional.ofNullable(getJda.getTextChannelById(schedulerChannelID));

        schedulerChannel.ifPresentOrElse(this::deletePlayer,
                () -> log.error("Scheduler channel with ID {} does not exist", schedulerChannelID));
    }

    private List<AdminServerGroup> getOutdatedVipPlayers(List<AdminServerGroup> outdatedPlayers) {
        return outdatedPlayers
                .stream()
                .filter(adminServerGroup -> {
                    var role = serverGroupService.getServerGroupName(adminServerGroup.getGroupID());
                    return VIP.equalsIgnoreCase(role);
                })
                .collect(Collectors.toList());
    }

    private List<AdminServerGroup> getOutdatedStaffPlayers(List<AdminServerGroup> outdatedPlayers) {
        return outdatedPlayers
                .stream()
                .filter(adminServerGroup -> {
                    var role = serverGroupService.getServerGroupName(adminServerGroup.getGroupID());
                    return MOD.equalsIgnoreCase(role) || ADMIN.equalsIgnoreCase(role);
                })
                .collect(Collectors.toList());
    }

    private void sendMessageToSchedulerChannel(TextChannel textChannel) {
        var outdatedPlayerRoles = adminServerGroupService.findOutdatedPlayersRolesWithOffsetDays(2);

        if (CollectionUtils.isNotEmpty(outdatedPlayerRoles)) {
            var pingStaff = getStaffPing();
            textChannel.sendMessage(pingStaff).queue();

            var deletionDate = LocalDate.now().plusDays(2).format(DD_MM_YYYY);
            var notifyRemovalMessage = String.format(
                    messageService.getMessage(JOB_NOTIFY_REMOVAL),
                    deletionDate, outdatedPlayerRoles.size());
            log.info(notifyRemovalMessage);
            textChannel.sendMessage(notifyRemovalMessage).queue();

            outdatedPlayerRoles.forEach(adminServerGroup -> {
                var playerInfo = playerInformation(adminServerGroup);
                var steamID = getSteamID(playerInfo);

                textChannel.sendMessageEmbeds(playerInfo).queue();
                log.info("Player with Steam ID {} has expiration date of {}", steamID, adminServerGroup.getExpirationDate());
            });
        }
    }

    private void sendMessageToReminderChannel(TextChannel textChannel, List<AdminServerGroup> outdatedPlayers) {
        if (CollectionUtils.isNotEmpty(outdatedPlayers)) {
            outdatedPlayers.forEach(adminServerGroup -> {
                var playerToNotify = sourcebanPlayerService.findPlayerByUserID(adminServerGroup.getAdminID());

                playerToNotify.ifPresent(player -> {
                    if (StringUtils.isNotBlank(player.getDiscordID())) {
                        var pingUser = String.format("<@%s>", player.getDiscordID());
                        var role = serverGroupService.getServerGroupName(adminServerGroup.getGroupID());
                        var server = serverService
                                .findServerNameByServerIDOrServerName(String.valueOf(adminServerGroup.getServerID()))
                                .orElse(UNKNOWN);
                        var expirationDate = adminServerGroup.getExpirationDate().format(DD_MM_YYYY);
                        var deletionDate = LocalDate.now().plusDays(2).format(DD_MM_YYYY);

                        var notifyUserMessage = String.format(
                                messageService.getMessage(JOB_NOTIFY_PLAYER_REMOVAL),
                                pingUser, role, server, expirationDate, deletionDate);

                        textChannel.sendMessage(notifyUserMessage).queue();
                        log.info("Reminding user {} to update his/her {} role in {} server", pingUser, role, server);
                    }
                });
            });
        }
    }

    private void deletePlayer(TextChannel textChannel) {
        var outdatedPlayerRoles = adminServerGroupService.findOutdatedPlayersRolesWithOffsetDays(0);

        if (CollectionUtils.isNotEmpty(outdatedPlayerRoles)) {
            var pingStaff = getStaffPing();
            var message = String.format(messageService.getMessage(JOB_DELETING_PLAYERS),
                    outdatedPlayerRoles.size());
            textChannel.sendMessage(pingStaff).queue();
            textChannel.sendMessage(message).queue();

            var serverGuild = Optional.ofNullable(getJda.getGuildById(guildID));

            if (serverGuild.isPresent()) {
                var guild = serverGuild.get();
                var rolesToRemove = vipAndStaffRoles.stream()
                        .map(guild::getRoleById)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                outdatedPlayerRoles.forEach(adminServerGroup -> {
                    var playerToDelete = sourcebanPlayerService.findPlayerByUserID(adminServerGroup.getAdminID());

                    playerToDelete.ifPresent(player -> {
                        var deleteMessages = deletePlayerFromDatabase(adminServerGroup, player);

                        Consumer<Throwable> throwableConsumer = throwable -> log.error(throwable.getMessage());
                        Consumer<EmbedBuilder> embedBuilderConsumer = embedBuilder -> {
                            deleteMessages.add(embedBuilder.build());
                            textChannel.sendMessageEmbeds(deleteMessages).queue();
                        };

                        deleteDiscordPlayerRoles(guild, player, rolesToRemove, embedBuilderConsumer, throwableConsumer);
                    });
                });
            } else {
                log.error("Guild ID {} does not exists. Cannot delete outdated players", guildID);
            }
        }
    }

    private void deleteDiscordPlayerRoles(Guild guild, Player player, List<Role> rolesToRemove,
                                          Consumer<EmbedBuilder> embedBuilderConsumer, Consumer<Throwable> throwableConsumer) {
        var playerDiscordID = player.getDiscordID();
        var embedBuilder = new EmbedBuilder();

        if (StringUtils.isNotBlank(playerDiscordID)) {
            guild.retrieveMemberById(playerDiscordID)
                    .queue(member -> {
                                var title = String.format(messageService.getMessage(DISCORD_DELETED_ROLES_FOR),
                                        member.getEffectiveName());
                                embedBuilder.setTitle(title);
                                embedBuilder.setColor(Color.GREEN);
                                guild.modifyMemberRoles(member, null, rolesToRemove).queue();
                                embedBuilderConsumer.accept(embedBuilder);
                            },
                            throwable -> {
                                embedBuilder.setTitle(messageService.getMessage(DISCORD_INVALID_USER));
                                embedBuilder.setColor(Color.ORANGE);
                                embedBuilderConsumer.accept(embedBuilder);
                                throwableConsumer.accept(throwable);
                            });
        } else {
            embedBuilder.setTitle(messageService.getMessage(DISCORD_EMPTY_DISCORD_ID));
            embedBuilder.setColor(Color.YELLOW);
            embedBuilderConsumer.accept(embedBuilder);
        }
    }

    private List<MessageEmbed> deletePlayerFromDatabase(AdminServerGroup adminServerGroup, Player player) {
        log.info("Deleting Player {} from databases with AdminServerGroup {}", player, adminServerGroup);
        var server = serverService
                .findServerNameByServerIDOrServerName(String.valueOf(adminServerGroup.getServerID()))
                .orElse(UNKNOWN);

        var deletingPlayerMessage = String.format(
                messageService.getMessage(JOB_DELETING_PLAYER),
                player.getUsername(), player.getSteamID(), server
        );

        var messageEmbed = new EmbedBuilder();
        messageEmbed.addField(deletingPlayerMessage, "", false);

        var deletingPlayerEmbeds = new ArrayList<MessageEmbed>();
        deletingPlayerEmbeds.add(messageEmbed.build());
        deletingPlayerEmbeds.addAll(playerService.deletePlayer(player.getSteamID(), String.valueOf(adminServerGroup.getServerID())));

        return deletingPlayerEmbeds;
    }

    private String getSteamID(MessageEmbed playerInfo) {
        return playerInfo.getFields()
                .stream()
                .filter(field -> "SteamID".equalsIgnoreCase(field.getName()))
                .findFirst()
                .map(MessageEmbed.Field::getValue)
                .orElse(UNKNOWN);
    }

    private String getStaffPing() {
        return String.format("<@&%s>", staffRoleID);
    }

    private MessageEmbed playerInformation(AdminServerGroup adminServerGroup) {
        var messageEmbed = new EmbedBuilder();
        var adminID = adminServerGroup.getAdminID();
        var serverID = adminServerGroup.getServerID();
        var server = serverService.findServerNameByServerIDOrServerName(String.valueOf(serverID));
        var playerToDelete = sourcebanPlayerService.findPlayerByUserID(adminID);
        var role = serverGroupService.getServerGroupName(adminServerGroup.getGroupID());

        playerToDelete.ifPresent(player -> {
            var expirationDate = Optional.ofNullable(adminServerGroup.getExpirationDate())
                    .map(localDate -> localDate.format(DD_MM_YYYY))
                    .orElse(UNKNOWN);

            Optional.ofNullable(player.getDiscordID())
                    .ifPresent(id -> messageEmbed.addField("Discord", String.format("<@%s>", id), false));

            messageEmbed.addField("Username", player.getUsername(), false);
            messageEmbed.addField("SteamID", player.getSteamID(), false);
            messageEmbed.addField("Server", server.orElse(UNKNOWN), false);
            messageEmbed.addField("Role", role, false);
            messageEmbed.addField("Expiration Date", expirationDate, false);
            messageEmbed.setColor(Color.DARK_GRAY);
        });

        return messageEmbed.build();
    }
}
