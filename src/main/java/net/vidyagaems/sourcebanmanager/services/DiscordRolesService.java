package net.vidyagaems.sourcebanmanager.services;

import java.util.List;

public interface DiscordRolesService {

    boolean hasStaffRole(List<String> roles);
}
