package net.vidyagaems.sourcebanmanager.services;

import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.Player;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;

import java.util.Optional;

public interface SourceBanPlayerService {

    MessageEmbed savePlayer(PlayerModel playerModel);
    MessageEmbed deletePlayer(String steamID);
    MessageEmbed renewPlayerExpirationDate(int monthsToAdd, String steamID, Integer serverID);
    Player findBySteamID(String steamID);
    Optional<Player> findPlayerBySteamID(String steamID);
    String findSteamIDFromParam(String parameter);
    Optional<Player> findPlayerByUsername(String username);
    Optional<Player> findPlayerByUserID(Integer userID);
}
