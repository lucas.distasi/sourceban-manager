package net.vidyagaems.sourcebanmanager.services;

import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.Map;

public interface EmbedService {

    Map<String, MessageEmbed> getAllEmbeds();
    MessageEmbed getSBMMessageEmbed();
    MessageEmbed getErrorMessageEmbed(String error);
    MessageEmbed getMessageEmbedForInexistentPlayer(String playerName);
    MessageEmbed getInvalidParameterEmbed(String title);
}
