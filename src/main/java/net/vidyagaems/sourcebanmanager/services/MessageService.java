package net.vidyagaems.sourcebanmanager.services;

public interface MessageService {

    String getMessage(String property);
}
