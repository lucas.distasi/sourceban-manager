package net.vidyagaems.sourcebanmanager.services;

import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.ServerGroup;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;

import java.util.List;
import java.util.Optional;

public interface ServerGroupService {

    MessageEmbed saveRole(PlayerModel playerModel);
    MessageEmbed deletePlayerRoles(String steamID, String server);
    List<ServerGroup> findAllServerGroups();
    String getServerGroupName(Integer serverGroupID);
    Integer getServerGroupID(String serverGroupName);
    Optional<ServerGroup> getServerGroup(String roleServer);
}
