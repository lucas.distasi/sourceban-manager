package net.vidyagaems.sourcebanmanager.services;

import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;

import java.util.List;
import java.util.Optional;

public interface AdminServerGroupService {

    void save(AdminServerGroup adminServerGroup);
    Optional<List<AdminServerGroup>> findPlayerServerGroups(Integer adminID);
    Optional<AdminServerGroup> findAdminServerGroupByAdminIDAndServerID(Integer adminID, Integer serverID);
    void deleteAllByAdminIDAndServerID(Integer adminID, Integer serverID);
    List<AdminServerGroup> findOutdatedPlayersRolesWithOffsetDays(int days);
    void updatePlayerExpirationDate(int monthsToAdd, int adminID, Integer serverID);
}
