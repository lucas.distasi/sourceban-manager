package net.vidyagaems.sourcebanmanager.services;

public interface JDAService {

    boolean isUserInServer(String discordID);
}
