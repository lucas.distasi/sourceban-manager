package net.vidyagaems.sourcebanmanager.services.mappings.impl;

import net.vidyagaems.sourcebanmanager.entities.Player;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.services.mappings.EntityMapperService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class SourceBanPlayerMapperService implements EntityMapperService<Player, PlayerModel> {

    @Override
    public Player mapFrom(PlayerModel playerModel) {
        var discordID = "0".equalsIgnoreCase(playerModel.getDiscordID()) ?
                null :
                playerModel.getDiscordID();

        var player = new Player();
        player.setUsername(Optional.ofNullable(playerModel.getUsername()).orElse(UUID.randomUUID().toString()));
        player.setSteamID(playerModel.getSteamID());
        player.setServerGroup(playerModel.getRoleServer().getRole());
        player.setDiscordID(discordID);
        setPlayerDefaultValues(player);

        return player;
    }

    private void setPlayerDefaultValues(Player player) {
        player.setExtraFlags(0);
        player.setImmunity(0);
        player.setFlags("");
        player.setPassword("");
        player.setGroupID(-1);
        player.setEmail("");
    }
}
