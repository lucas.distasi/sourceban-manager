package net.vidyagaems.sourcebanmanager.services.mappings;

public interface EntityMapperService<U, T> {

    U mapFrom(T from);
}
