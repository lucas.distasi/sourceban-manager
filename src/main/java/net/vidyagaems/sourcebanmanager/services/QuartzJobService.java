package net.vidyagaems.sourcebanmanager.services;

public interface QuartzJobService {

    void notifyRemovalOfOutdatedPlayers();
    void removeOutdatedVips();
}
