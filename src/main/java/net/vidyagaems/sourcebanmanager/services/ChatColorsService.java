package net.vidyagaems.sourcebanmanager.services;

import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.entities.CustomChatColor;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;

import java.util.Optional;

public interface ChatColorsService {

    MessageEmbed saveChatColor(PlayerModel playerModel);
    Optional<CustomChatColor> findTurbineChatColorBySteamID(String steamID);
    Optional<CustomChatColor> findOrangeChatColorBySteamID(String steamID);
    MessageEmbed deletePlayerColors(String steamID, String server);
}
