package net.vidyagaems.sourcebanmanager.services;

import net.dv8tion.jda.api.entities.MessageEmbed;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;

import java.util.List;

public interface PlayerService {

    List<MessageEmbed> savePlayerWithRoleAndChatColor(PlayerModel playerModel);
    MessageEmbed getPlayerInformation(String parameter);
    boolean nonRootPlayerExistsAndHasRoles(String parameter);
    List<MessageEmbed> deletePlayer(String steamID, String server);
}
