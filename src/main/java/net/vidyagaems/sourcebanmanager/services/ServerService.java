package net.vidyagaems.sourcebanmanager.services;

import net.vidyagaems.sourcebanmanager.entities.Server;

import java.util.List;
import java.util.Optional;

public interface ServerService {

    List<Server> findAll();
    Optional<String> findServerNameByServerIDOrServerName(String param);
    Optional<Integer> findServerIDByServerNameOrServerID(String param);
    List<Server> findServersForAdminID(Integer adminID);
}
