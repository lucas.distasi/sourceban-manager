package net.vidyagaems.sourcebanmanager.appenders;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.time.LocalDateTime;
import java.util.Optional;

import static net.vidyagaems.sourcebanmanager.utils.DateUtils.DD_MM_YYYY_HH_MM_SS;

@Slf4j
@Component
@RequiredArgsConstructor
public class ErrorAppender extends UnsynchronizedAppenderBase<ILoggingEvent> implements SmartLifecycle {

    @Value("${discord.channel.errors}")
    private String errorsChannelID;

    @Value("${discord.role.developer}")
    private String developerRoleID;

    private final JDA getJda;

    @Override
    protected void append(ILoggingEvent eventObject) {
        if (Level.ERROR.equals(eventObject.getLevel())) {
            log.error("An unexpected exception occurred");

            var errorMessageEmbed = new EmbedBuilder();
            errorMessageEmbed.addField("Application", "sourcebanmanager", false);
            errorMessageEmbed.addField("Timestamp", LocalDateTime.now().format(DD_MM_YYYY_HH_MM_SS), false);
            errorMessageEmbed.setColor(Color.RED);

            var formattedMessage = eventObject.getFormattedMessage();
            var loggerName = eventObject.getLoggerName();
            var throwableProxy = eventObject.getThrowableProxy();

            Optional.ofNullable(formattedMessage)
                    .ifPresent(message -> errorMessageEmbed.addField("Message", message, false));
            Optional.ofNullable(loggerName)
                    .ifPresent(logger -> errorMessageEmbed.addField("Logger Name", logger, false));
            Optional.ofNullable(throwableProxy)
                    .flatMap(throwable -> Optional.ofNullable(throwableProxy.getClassName()))
                    .ifPresent(className -> errorMessageEmbed.addField("Class Name", className, false));

            Optional.ofNullable(getJda.getTextChannelById(errorsChannelID))
                    .ifPresentOrElse(textChannel -> {
                        textChannel.sendMessage(String.format("<@&%s>", developerRoleID)).queue();
                        textChannel.sendMessageEmbeds(errorMessageEmbed.build()).queue();
                    }, () -> log.error("Errors channel with ID {} does not exist", errorsChannelID));
        }
    }

    @Override
    public boolean isRunning() {
        return isStarted();
    }
}
