package net.vidyagaems.sourcebanmanager.models;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

@Data
@Builder
public class BanModel {

    private String steamID;
    private String ip;
    private String country;
    private String name;
    private LocalDateTime banDate;
    private LocalDateTime banFinishDate;
    private String reason;
    private String admin;
    private String banStatus;
    private String removedBy;
    private LocalDateTime removedOn;
    private String unbanReason;

    public String getBanStatus() {
        return StringUtils.isBlank(getUnbanReason()) ?
                "EXPIRED" :
                "ACTIVE";
    }
}
