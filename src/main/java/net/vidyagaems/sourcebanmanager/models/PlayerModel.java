package net.vidyagaems.sourcebanmanager.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class PlayerModel {

    private String username;
    private String steamID;
    private RoleServer roleServer;
    private String tagName;
    private String tagColor;
    private Long vipDuration;
    private LocalDate dateAdded;
    private LocalDate expirationDate;
    private String discordID;

}
