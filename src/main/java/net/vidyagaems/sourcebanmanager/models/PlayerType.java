package net.vidyagaems.sourcebanmanager.models;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public enum PlayerType {

    VIP(Arrays.asList("!sbm addvip")),
    MOD(Arrays.asList("!sbm addmod")),
    ADMIN(Arrays.asList("!sbm addadmin"));

    private final List<String> commands;

    PlayerType(List<String> commands) {
        this.commands = commands;
    }

}
