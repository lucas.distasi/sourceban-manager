package net.vidyagaems.sourcebanmanager.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleServer {

    private String role;
    private Integer server;

}
