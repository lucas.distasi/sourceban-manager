package net.vidyagaems.sourcebanmanager.models.questions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;

@Data
@EqualsAndHashCode(callSuper = true)
public class AddPlayerListenerQuestionModel extends PlayerQuestionModel {

    private PlayerModel playerModel;

    public AddPlayerListenerQuestionModel(TextChannel textChannel, Message message, User author, PlayerModel playerModel) {
        super(textChannel, message, author);
        this.playerModel = playerModel;
    }
}
