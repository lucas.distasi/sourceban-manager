package net.vidyagaems.sourcebanmanager.models.questions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

@Data
@EqualsAndHashCode(callSuper = true)
public class RenewPlayerListenerQuestionModel extends PlayerQuestionModel {

    private String steamID;
    private String server;

    public RenewPlayerListenerQuestionModel(TextChannel textChannel, Message message, User author,
                                            String steamID, String server) {
        super(textChannel, message, author);
        this.steamID = steamID;
        this.server = server;
    }
}
