package net.vidyagaems.sourcebanmanager.models.questions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerQuestionModel {

    private TextChannel textChannel;
    private Message message;
    private User author;
}
