package net.vidyagaems.sourcebanmanager.listeners.player;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vidyagaems.sourcebanmanager.models.questions.PlayerQuestionModel;
import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import org.owasp.security.logging.SecurityMarkers;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.concurrent.TimeUnit;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.isViewPlayerCommand;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ERROR_REACTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.EVENT_WAITER_TIMEOUT;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_USERNAME_STEAM_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_USERNAME_STEAM_ID_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.SUCCESS_REACTION;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.addUserToSet;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.isExecutingCommand;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.removeUserFromSet;
import static net.vidyagaems.sourcebanmanager.utils.Utils.getRoles;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isInvalidAuthor;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidSteamID;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidUsername;

@Slf4j
@Component
@RequiredArgsConstructor
public class ViewPlayerListener extends ListenerAdapter {

    private final EventWaiter eventWaiter;
    private final EmbedService embedService;
    private final PlayerService playerService;
    private final MessageService messageService;
    private final DiscordRolesService discordRolesService;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        var textChannel = event.getChannel();
        var message = event.getMessage();
        var author = message.getAuthor();

        if (isViewPlayerCommand(message.getContentRaw()) &&
                !isExecutingCommand(author.getId())) {
            var member = event.getGuild().getMember(author);

            if (discordRolesService.hasStaffRole(getRoles(member))) {
                addUserToSet(author.getId());
                askSteamIDOrUsername(new PlayerQuestionModel(textChannel, message, author));
            }
        }
    }

    private void askSteamIDOrUsername(PlayerQuestionModel playerQuestionModel) {
        var textChannel = playerQuestionModel.getTextChannel();
        var message = playerQuestionModel.getMessage();
        var author = playerQuestionModel.getAuthor();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_USERNAME_STEAM_ID_QUESTION));
        messageEmbed.setColor(Color.CYAN);

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var param = messageFromEvent.getContentRaw();

                            if (isValidSteamID(param) || isValidUsername(param)) {
                                addSuccessReaction(messageFromEvent);

                                return true;
                            } else {
                                addErrorReaction(messageFromEvent);
                                var title = messageService.getMessage(INVALID_USERNAME_STEAM_ID);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();

                                return false;
                            }
                        },
                        messageEvent -> {
                            var messageFromEvent = messageEvent.getMessage();
                            var param = messageFromEvent.getContentRaw();
                            log.info("User {} with name {}, retrieving info for player with param {}", author.getAsMention(), author.getName(), param);

                            messageFromEvent.replyEmbeds(playerService.getPlayerInformation(param)).queue();

                            removeUserFromSet(author.getId());
                            log.info(SecurityMarkers.EVENT_SUCCESS, "User {} with name {}, retrieved info for player with param {}", author.getAsMention(), author.getName(), param);
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            removeUserFromSet(author.getId());
                            pingUserIfTimeIsOut(textChannel, author);
                        }
                ));
    }

    private void pingUserIfTimeIsOut(TextChannel textChannel, User author) {
        var message = String.format(messageService.getMessage(EVENT_WAITER_TIMEOUT), author.getId());
        textChannel.sendMessage(message).queue();
    }

    private void addSuccessReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(SUCCESS_REACTION).queue();
    }

    private void addErrorReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(ERROR_REACTION).queue();
    }
}
