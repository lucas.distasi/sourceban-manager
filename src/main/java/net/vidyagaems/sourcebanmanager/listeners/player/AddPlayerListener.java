package net.vidyagaems.sourcebanmanager.listeners.player;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.models.PlayerModel;
import net.vidyagaems.sourcebanmanager.models.RoleServer;
import net.vidyagaems.sourcebanmanager.models.questions.AddPlayerListenerQuestionModel;
import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.JDAService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import static net.vidyagaems.sourcebanmanager.constants.Constants.ADMIN;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ADMIN_DEFAULT_TAG;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ADMIN_DEFAULT_TAG_NAME_COLOR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.isAddPlayerCommand;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ERROR_REACTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.MOD;
import static net.vidyagaems.sourcebanmanager.constants.Constants.MOD_DEFAULT_TAG;
import static net.vidyagaems.sourcebanmanager.constants.Constants.MOD_DEFAULT_TAG_NAME_COLOR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ORANGE_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.EVENT_WAITER_TIMEOUT;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_DISCORD_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_HEX_COLOR;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_MONTHS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_SERVER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_STEAM_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_TAG_NAME;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_USERNAME;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_DISCORD_ID_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_DURATION_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_IS_NOT_IN_DISCORD_SERVER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_SERVER_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_STEAM_ID_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_TAG_COLOR_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_TAG_NAME_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_USERNAME_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.USERNAME_EXISTS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.SUCCESS_REACTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.VIP;
import static net.vidyagaems.sourcebanmanager.constants.Constants.VIP_DEFAULT_TAG;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.addUserToSet;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.isExecutingCommand;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.removeUserFromSet;
import static net.vidyagaems.sourcebanmanager.utils.Utils.getPlayerType;
import static net.vidyagaems.sourcebanmanager.utils.Utils.getRoles;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isInvalidAuthor;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidAmountOfMonths;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidDiscordID;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidHexColor;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidServer;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidSteamID;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidTagName;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidUsername;

@Slf4j
@Component
@RequiredArgsConstructor
public class AddPlayerListener extends ListenerAdapter {

    private final JDAService jdaService;
    private final EventWaiter eventWaiter;
    private final EmbedService embedService;
    private final PlayerService playerService;
    private final ServerService serverService;
    private final MessageService messageService;
    private final DiscordRolesService discordRolesService;
    private final SourceBanPlayerService sourcebanPlayerService;

    @Value("${staff.tolerance-days}")
    private Integer staffToleranceDays;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        var textChannel = event.getChannel();
        var message = event.getMessage();
        var author = message.getAuthor();

        if (isAddPlayerCommand(message.getContentRaw()) &&
                !isExecutingCommand(author.getId())) {
            var member = event.getGuild().getMember(author);

            if (discordRolesService.hasStaffRole(getRoles(member))) {
                addUserToSet(author.getId());
                askSteamID(new AddPlayerListenerQuestionModel(textChannel, message, author, new PlayerModel()));
            }
        }
    }

    private void askSteamID(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();
        var playerType = getPlayerType(message.getContentRaw());

        playerModel.setRoleServer(RoleServer.builder()
                .role(playerType)
                .build());

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_STEAM_ID_QUESTION));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var steamID = messageFromEvent.getContentRaw();

                            if (isValidSteamID(steamID)) {
                                addSuccessReaction(messageFromEvent);
                                playerModel.setSteamID(steamID);

                                return true;
                            } else {
                                var title = messageService.getMessage(INVALID_STEAM_ID);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> {
                            if (isStaff(playerType))
                                askStaffUsername(addPlayerListenerQuestionModel);
                            else
                                askServer(addPlayerListenerQuestionModel);
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void askStaffUsername(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_USERNAME_QUESTION));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var username = messageFromEvent.getContentRaw();
                            if (!isValidUsername(username)) {
                                var title = messageService.getMessage(INVALID_USERNAME);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }

                            if (sourcebanPlayerService.findPlayerByUsername(username).isPresent()) {
                                var title = messageService.getMessage(USERNAME_EXISTS);
                                messageFromEvent.replyEmbeds(embedService.getErrorMessageEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            } else {
                                addSuccessReaction(messageFromEvent);
                                playerModel.setUsername(username);
                                addPlayerListenerQuestionModel.setMessage(messageFromEvent);

                                return true;
                            }
                        },
                        messageEvent -> askServer(addPlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void askServer(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();
        var servers = serverService.findAll();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_SERVER_QUESTION));

        servers.forEach(server -> {
            var serverID = String.format("[%s]", server.getServerID());
            messageEmbed.addField(serverID, server.getServerName(), true);
        });

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var server = messageFromEvent.getContentRaw();

                            if (isValidServer(server, servers)) {
                                addSuccessReaction(messageFromEvent);
                                addPlayerListenerQuestionModel.setMessage(messageFromEvent);

                                return true;
                            } else {
                                addErrorReaction(messageFromEvent);
                                var title = messageService.getMessage(INVALID_SERVER);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();

                                return false;
                            }
                        },
                        messageEvent -> {
                            var messageFromEvent = messageEvent.getMessage();
                            var server = servers
                                    .stream()
                                    .filter(s -> messageFromEvent.getContentRaw().equalsIgnoreCase(s.getServerName()) ||
                                            messageFromEvent.getContentRaw().equalsIgnoreCase(String.valueOf(s.getServerID())))
                                    .map(Server::getServerID)
                                    .findFirst()
                                    .orElse(ORANGE_ID);

                            var role = playerModel.getRoleServer().getRole();

                            playerModel.setRoleServer(RoleServer
                                    .builder()
                                    .role(role)
                                    .server(server)
                                    .build());

                            setDefaultValuesForRole(addPlayerListenerQuestionModel);
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void setDefaultValuesForRole(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();
        var role = playerModel.getRoleServer().getRole();

        if (MOD.equalsIgnoreCase(role)) {
            playerModel.setTagName(MOD_DEFAULT_TAG);
            playerModel.setTagColor(MOD_DEFAULT_TAG_NAME_COLOR);
            playerModel.setExpirationDate(LocalDate.now().plusDays(staffToleranceDays));

            askDiscordID(addPlayerListenerQuestionModel);
        }

        if (ADMIN.equalsIgnoreCase(role)) {
            playerModel.setTagName(ADMIN_DEFAULT_TAG);
            playerModel.setTagColor(ADMIN_DEFAULT_TAG_NAME_COLOR);
            playerModel.setExpirationDate(LocalDate.now().plusDays(staffToleranceDays));

            askDiscordID(addPlayerListenerQuestionModel);
        }

        if (VIP.equalsIgnoreCase(role)) {
            playerModel.setExpirationDate(LocalDate.now());

            askTagName(addPlayerListenerQuestionModel);
        }
    }

    private void askTagName(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_TAG_NAME_QUESTION));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var tagName = messageFromEvent.getContentRaw();

                            if (isValidTagName(tagName)) {
                                addSuccessReaction(messageFromEvent);
                                addPlayerListenerQuestionModel.setMessage(messageFromEvent);
                                tagName = tagName.equalsIgnoreCase("S") ? VIP_DEFAULT_TAG : tagName;
                                playerModel.setTagName(tagName);

                                return true;
                            } else {
                                var title = messageService.getMessage(INVALID_TAG_NAME);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> askTagColor(addPlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void askTagColor(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_TAG_COLOR_QUESTION));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var tagColor = messageFromEvent.getContentRaw();

                            if (isValidHexColor(tagColor)) {
                                addSuccessReaction(messageFromEvent);
                                playerModel.setTagColor(tagColor);
                                addPlayerListenerQuestionModel.setMessage(messageFromEvent);

                                return true;
                            } else {
                                var title = messageService.getMessage(INVALID_HEX_COLOR);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> askDiscordID(addPlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void askDiscordID(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_DISCORD_ID_QUESTION));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var discordID = messageFromEvent.getContentRaw();

                            if (!isValidDiscordID(discordID)) {
                                var title = messageService.getMessage(INVALID_DISCORD_ID);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }

                            if (jdaService.isUserInServer(discordID)) {
                                addSuccessReaction(messageFromEvent);
                                playerModel.setDiscordID(discordID);
                                addPlayerListenerQuestionModel.setMessage(messageFromEvent);

                                return true;
                            } else {
                                var title = messageService.getMessage(PLAYER_IS_NOT_IN_DISCORD_SERVER);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> askRoleDuration(addPlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private void askRoleDuration(AddPlayerListenerQuestionModel addPlayerListenerQuestionModel) {
        var textChannel = addPlayerListenerQuestionModel.getTextChannel();
        var message = addPlayerListenerQuestionModel.getMessage();
        var author = addPlayerListenerQuestionModel.getAuthor();
        var playerModel = addPlayerListenerQuestionModel.getPlayerModel();
        var playerRole = playerModel.getRoleServer().getRole();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(String.format(messageService.getMessage(PLAYER_DURATION_QUESTION), playerRole));

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, author)) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var months = messageFromEvent.getContentRaw();

                            if (isValidAmountOfMonths(months)) {
                                addSuccessReaction(messageFromEvent);
                                playerModel.setDateAdded(LocalDate.now());

                                return true;
                            } else {
                                var title = messageService.getMessage(INVALID_MONTHS);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> {
                            var messageFromEvent = messageEvent.getMessage();
                            var monthsToAdd = Long.parseLong(messageFromEvent.getContentRaw());
                            var originalFinishDate = playerModel.getExpirationDate();

                            playerModel.setVipDuration(monthsToAdd);
                            playerModel.setExpirationDate(originalFinishDate.plusMonths(monthsToAdd));

                            var responseMessages = playerService.savePlayerWithRoleAndChatColor(playerModel);

                            messageFromEvent.replyEmbeds(responseMessages).queue();
                            removeUserFromSet(author.getId());
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            pingUserIfTimeIsOut(textChannel, author);
                            removeUserFromSet(author.getId());
                        }
                ));
    }

    private boolean isStaff(String playerType) {
        return ADMIN.equalsIgnoreCase(playerType) || MOD.equalsIgnoreCase(playerType);
    }

    private void pingUserIfTimeIsOut(TextChannel textChannel, User author) {
        var message = String.format(messageService.getMessage(EVENT_WAITER_TIMEOUT), author.getId());
        textChannel.sendMessage(message).queue();
    }

    private void addSuccessReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(SUCCESS_REACTION).queue();
    }

    private void addErrorReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(ERROR_REACTION).queue();
    }
}
