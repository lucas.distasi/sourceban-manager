package net.vidyagaems.sourcebanmanager.listeners.player;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.models.questions.DeletePlayerListenerQuestionModel;
import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.apache.commons.lang3.math.NumberUtils;
import org.owasp.security.logging.SecurityMarkers;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.isDeletePlayerCommand;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ERROR_REACTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.*;
import static net.vidyagaems.sourcebanmanager.constants.Constants.SUCCESS_REACTION;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.addUserToSet;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.isExecutingCommand;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.removeUserFromSet;
import static net.vidyagaems.sourcebanmanager.utils.Utils.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeletePlayerListener extends ListenerAdapter {

    private final EventWaiter eventWaiter;
    private final EmbedService embedService;
    private final ServerService serverService;
    private final PlayerService playerService;
    private final MessageService messageService;
    private final DiscordRolesService discordRolesService;
    private final SourceBanPlayerService sourcebanPlayerService;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        var textChannel = event.getChannel();
        var message = event.getMessage();
        var author = message.getAuthor();

        if (isDeletePlayerCommand(message.getContentRaw()) &&
                !isExecutingCommand(author.getId())) {
            var member = event.getGuild().getMember(author);

            if (discordRolesService.hasStaffRole(getRoles(member))) {
                addUserToSet(author.getId());
                askSteamIDOrUsername(new DeletePlayerListenerQuestionModel(textChannel, message, author, null, null));
            }
        }
    }

    private void askSteamIDOrUsername(DeletePlayerListenerQuestionModel deletePlayerListenerQuestionModel) {
        var textChannel = deletePlayerListenerQuestionModel.getTextChannel();
        var message = deletePlayerListenerQuestionModel.getMessage();
        var author = deletePlayerListenerQuestionModel.getAuthor();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_USERNAME_STEAM_ID_QUESTION));
        messageEmbed.setColor(Color.ORANGE);

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var param = messageFromEvent.getContentRaw();

                            if (!isValidSteamID(param) && !isValidUsername(param)) {
                                addErrorReaction(messageFromEvent);
                                var title = messageService.getMessage(INVALID_USERNAME_STEAM_ID);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();

                                return false;
                            }

                            if (playerService.nonRootPlayerExistsAndHasRoles(param)) {
                                addSuccessReaction(messageFromEvent);
                                var steamID = sourcebanPlayerService.findSteamIDFromParam(messageFromEvent.getContentRaw());

                                messageFromEvent.replyEmbeds(playerService.getPlayerInformation(steamID)).queue();
                                deletePlayerListenerQuestionModel.setMessage(messageFromEvent);
                                deletePlayerListenerQuestionModel.setSteamID(steamID);

                                return true;
                            } else {
                                addErrorReaction(messageFromEvent);
                                messageFromEvent.replyEmbeds(embedService.getMessageEmbedForInexistentPlayer(param))
                                        .queue();

                                return false;
                            }
                        },
                        messageEvent -> askServer(deletePlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            removeUserFromSet(author.getId());
                            pingUserIfTimeIsOut(textChannel, author);
                        }
                ));
    }

    private void askServer(DeletePlayerListenerQuestionModel deletePlayerListenerQuestionModel) {
        var textChannel = deletePlayerListenerQuestionModel.getTextChannel();
        var message = deletePlayerListenerQuestionModel.getMessage();
        var author = deletePlayerListenerQuestionModel.getAuthor();
        var steamID = deletePlayerListenerQuestionModel.getSteamID();
        var player = sourcebanPlayerService.findPlayerBySteamID(steamID);

        List<Server> servers;

        if (player.isPresent()) {
            servers = serverService.findServersForAdminID(player.get().getUserID());

            var messageEmbed = new EmbedBuilder();
            messageEmbed.setTitle(messageService.getMessage(PLAYER_SERVER_DELETION));
            messageEmbed.setColor(Color.ORANGE);

            servers.forEach(server -> {
                var serverNumber = String.format("[%s]", server.getServerID());
                messageEmbed.addField(serverNumber, server.getServerName(), true);
            });

            message.replyEmbeds(messageEmbed.build())
                    .queue(m -> eventWaiter.waitForEvent(
                            GuildMessageReceivedEvent.class,
                            messageEvent -> {
                                if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                                var messageFromEvent = messageEvent.getMessage();
                                var server = messageFromEvent.getContentRaw();

                                if (isValidServer(server, servers)) {
                                    return true;
                                } else {
                                    var title = messageService.getMessage(INVALID_SERVER);
                                    messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                    addErrorReaction(messageFromEvent);

                                    return false;
                                }
                            },
                            messageEvent -> {
                                var messageFromEvent = messageEvent.getMessage();
                                var server = messageFromEvent.getContentRaw();

                                if (NumberUtils.isDigits(server)) {
                                    serverService.findServerNameByServerIDOrServerName(server)
                                            .ifPresentOrElse(serverName -> {
                                                addSuccessReaction(messageFromEvent);

                                                deletePlayerListenerQuestionModel.setMessage(messageFromEvent);
                                                deletePlayerListenerQuestionModel.setServer(server);

                                                askConfirmation(deletePlayerListenerQuestionModel);
                                            }, () -> {
                                                addErrorReaction(messageFromEvent);
                                                var title = String.format(messageService.getMessage(NO_SERVER_FOUND),
                                                        server);
                                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title))
                                                        .queue();
                                            });
                                } else {
                                    addSuccessReaction(messageFromEvent);

                                    deletePlayerListenerQuestionModel.setMessage(messageFromEvent);
                                    deletePlayerListenerQuestionModel.setServer(server);

                                    askConfirmation(deletePlayerListenerQuestionModel);
                                }
                            },
                            90, TimeUnit.SECONDS,
                            () -> {
                                removeUserFromSet(author.getId());
                                pingUserIfTimeIsOut(textChannel, author);
                            }
                    ));
        } else {
            var messageEmbed = new EmbedBuilder();
            messageEmbed.setTitle(String.format(messageService.getMessage(NO_PLAYER_FOUND), steamID));
            messageEmbed.setColor(Color.ORANGE);

            message.replyEmbeds(messageEmbed.build()).queue();
            removeUserFromSet(author.getId());
        }
    }

    private void askConfirmation(DeletePlayerListenerQuestionModel deletePlayerListenerQuestionModel) {
        var textChannel = deletePlayerListenerQuestionModel.getTextChannel();
        var message = deletePlayerListenerQuestionModel.getMessage();
        var author = deletePlayerListenerQuestionModel.getAuthor();
        var steamID = deletePlayerListenerQuestionModel.getSteamID();
        var server = deletePlayerListenerQuestionModel.getServer();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(String.format(messageService.getMessage(DELETE_PLAYER_CONFIRMATION_MESSAGE), steamID, server));
        messageEmbed.addField("", messageService.getMessage(YES_CONFIRMATION_MESSAGE), true);
        messageEmbed.addField("", messageService.getMessage(NO_CONFIRMATION_MESSAGE), true);
        messageEmbed.setColor(Color.ORANGE);

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();

                            if (isValidConfirmationMessage(messageFromEvent.getContentRaw())) {
                                 return true;
                            } else {
                                var title = messageService.getMessage(INVALID_CONFIRMATION_MESSAGE);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> {
                            List<MessageEmbed> responseMessages = new ArrayList<>();
                            var messageFromEvent = messageEvent.getMessage();

                            if (isAffirmativeConfirmationMessage(messageFromEvent.getContentRaw())) {
                                log.info("User {} with name {} is removing player with Steam ID {} from server {}", author.getAsMention(), author.getName(), steamID, server);
                                responseMessages = playerService.deletePlayer(steamID, server);
                            } else {
                                var title = String.format(messageService.getMessage(PLAYER_NOT_DELETED), steamID);
                                var deleteCancellationMessage = new EmbedBuilder();
                                deleteCancellationMessage.setTitle(title);
                                deleteCancellationMessage.setColor(Color.YELLOW);

                                messageFromEvent.replyEmbeds(deleteCancellationMessage.build()).queue();
                                log.info(SecurityMarkers.EVENT_SUCCESS, "User {} with name {} canceled removing player with Steam ID {} from server {}", author.getAsMention(), author.getName(), steamID, server);
                            }

                            messageFromEvent.replyEmbeds(responseMessages).queue();
                            addSuccessReaction(messageEvent.getMessage());
                            removeUserFromSet(author.getId());
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            removeUserFromSet(author.getId());
                            pingUserIfTimeIsOut(textChannel, author);
                        }
                ));
    }

    private void pingUserIfTimeIsOut(TextChannel textChannel, User author) {
        var message = String.format(messageService.getMessage(EVENT_WAITER_TIMEOUT), author.getId());
        textChannel.sendMessage(message).queue();
    }

    private void addSuccessReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(SUCCESS_REACTION).queue();
    }

    private void addErrorReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(ERROR_REACTION).queue();
    }
}
