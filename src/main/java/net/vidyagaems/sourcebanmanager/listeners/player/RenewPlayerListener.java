package net.vidyagaems.sourcebanmanager.listeners.player;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.models.questions.RenewPlayerListenerQuestionModel;
import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import net.vidyagaems.sourcebanmanager.services.PlayerService;
import net.vidyagaems.sourcebanmanager.services.ServerService;
import net.vidyagaems.sourcebanmanager.services.SourceBanPlayerService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.isRenewPlayerCommand;
import static net.vidyagaems.sourcebanmanager.constants.Constants.ERROR_REACTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.EVENT_WAITER_TIMEOUT;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_MONTHS;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_SERVER;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_USERNAME_STEAM_ID;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_PLAYER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.NO_SERVER_FOUND;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.PLAYER_USERNAME_STEAM_ID_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.UPDATE_EXPIRATION_DATE_MONTHS_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.UPDATE_EXPIRATION_DATE_QUESTION;
import static net.vidyagaems.sourcebanmanager.constants.Constants.SUCCESS_REACTION;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.addUserToSet;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.isExecutingCommand;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.removeUserFromSet;
import static net.vidyagaems.sourcebanmanager.utils.Utils.getRoles;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isInvalidAuthor;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidAmountOfMonths;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidServer;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidSteamID;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isValidUsername;

@Slf4j
@Component
@RequiredArgsConstructor
public class RenewPlayerListener extends ListenerAdapter {

    private final EventWaiter eventWaiter;
    private final EmbedService embedService;
    private final PlayerService playerService;
    private final ServerService serverService;
    private final MessageService messageService;
    private final DiscordRolesService discordRolesService;
    private final SourceBanPlayerService sourceBanPlayerService;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        var textChannel = event.getChannel();
        var message = event.getMessage();
        var author = message.getAuthor();

        if (isRenewPlayerCommand(message.getContentRaw()) &&
                !isExecutingCommand(author.getId())) {
            var member = event.getGuild().getMember(author);

            if (discordRolesService.hasStaffRole(getRoles(member))) {
                addUserToSet(author.getId());
                askSteamIDOrUsername(new RenewPlayerListenerQuestionModel(textChannel, message, author, null, null));
            }
        }
    }

    private void askSteamIDOrUsername(RenewPlayerListenerQuestionModel renewPlayerListenerQuestionModel) {
        var textChannel = renewPlayerListenerQuestionModel.getTextChannel();
        var message = renewPlayerListenerQuestionModel.getMessage();
        var author = renewPlayerListenerQuestionModel.getAuthor();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(PLAYER_USERNAME_STEAM_ID_QUESTION));
        messageEmbed.setColor(Color.CYAN);

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var param = messageFromEvent.getContentRaw();

                            if (!isValidSteamID(param) && !isValidUsername(param)) {
                                addErrorReaction(messageFromEvent);
                                var title = messageService.getMessage(INVALID_USERNAME_STEAM_ID);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();

                                return false;
                            }

                            if (playerService.nonRootPlayerExistsAndHasRoles(param)) {
                                var steamID = sourceBanPlayerService.findSteamIDFromParam(messageFromEvent.getContentRaw());
                                addSuccessReaction(messageFromEvent);
                                renewPlayerListenerQuestionModel.setMessage(messageFromEvent);
                                renewPlayerListenerQuestionModel.setSteamID(steamID);

                                return true;
                            } else {
                                addErrorReaction(messageFromEvent);
                                messageFromEvent.replyEmbeds(embedService.getMessageEmbedForInexistentPlayer(param))
                                        .queue();

                                return false;
                            }
                        },
                        messageEvent -> askServer(renewPlayerListenerQuestionModel),
                        90, TimeUnit.SECONDS,
                        () -> {
                            removeUserFromSet(author.getId());
                            pingUserIfTimeIsOut(textChannel, author);
                        }
                ));
    }

    private void askServer(RenewPlayerListenerQuestionModel renewPlayerListenerQuestionModel) {
        var textChannel = renewPlayerListenerQuestionModel.getTextChannel();
        var message = renewPlayerListenerQuestionModel.getMessage();
        var author = renewPlayerListenerQuestionModel.getAuthor();
        var steamID = renewPlayerListenerQuestionModel.getSteamID();
        var player = sourceBanPlayerService.findPlayerBySteamID(steamID);

        List<Server> servers;

        if (player.isPresent()) {
            servers = serverService.findServersForAdminID(player.get().getUserID());

            var messageEmbed = new EmbedBuilder();
            messageEmbed.setTitle(messageService.getMessage(UPDATE_EXPIRATION_DATE_QUESTION));
            messageEmbed.setColor(Color.DARK_GRAY);

            servers.forEach(server -> {
                var serverNumber = String.format("[%s]", server.getServerID());
                messageEmbed.addField(serverNumber, server.getServerName(), true);
            });

            message.replyEmbeds(messageEmbed.build())
                    .queue(m -> eventWaiter.waitForEvent(
                            GuildMessageReceivedEvent.class,
                            messageEvent -> {
                                if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                                var messageFromEvent = messageEvent.getMessage();
                                var server = messageFromEvent.getContentRaw();

                                if (isValidServer(server, servers)) {
                                    return true;
                                } else {
                                    var title = messageService.getMessage(INVALID_SERVER);
                                    messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                    addErrorReaction(messageFromEvent);

                                    return false;
                                }
                            },
                            messageEvent -> {
                                var messageFromEvent = messageEvent.getMessage();
                                var server = messageFromEvent.getContentRaw();

                                if (NumberUtils.isDigits(server)) {
                                    serverService.findServerNameByServerIDOrServerName(server)
                                            .ifPresentOrElse(serverName -> {
                                                addSuccessReaction(messageFromEvent);

                                                renewPlayerListenerQuestionModel.setMessage(messageFromEvent);
                                                renewPlayerListenerQuestionModel.setServer(server);

                                                askMonthsToAdd(renewPlayerListenerQuestionModel);
                                            }, () -> {
                                                addErrorReaction(messageFromEvent);
                                                var title = String.format(messageService.getMessage(NO_SERVER_FOUND),
                                                        server);
                                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title))
                                                        .queue();
                                            });
                                } else {
                                    addSuccessReaction(messageFromEvent);

                                    renewPlayerListenerQuestionModel.setMessage(messageFromEvent);
                                    renewPlayerListenerQuestionModel.setServer(server);

                                    askMonthsToAdd(renewPlayerListenerQuestionModel);
                                }
                            },
                            90, TimeUnit.SECONDS,
                            () -> {
                                removeUserFromSet(author.getId());
                                pingUserIfTimeIsOut(textChannel, author);
                            }
                    ));
        } else {
            var messageEmbed = new EmbedBuilder();
            messageEmbed.setTitle(String.format(messageService.getMessage(NO_PLAYER_FOUND), steamID));
            messageEmbed.setColor(Color.ORANGE);

            message.replyEmbeds(messageEmbed.build()).queue();
            removeUserFromSet(author.getId());
        }
    }

    private void askMonthsToAdd(RenewPlayerListenerQuestionModel renewPlayerListenerQuestionModel) {
        var textChannel = renewPlayerListenerQuestionModel.getTextChannel();
        var message = renewPlayerListenerQuestionModel.getMessage();
        var author = renewPlayerListenerQuestionModel.getAuthor();
        var steamID = renewPlayerListenerQuestionModel.getSteamID();
        var server = renewPlayerListenerQuestionModel.getServer();

        var messageEmbed = new EmbedBuilder();
        messageEmbed.setTitle(messageService.getMessage(UPDATE_EXPIRATION_DATE_MONTHS_QUESTION));
        messageEmbed.setColor(Color.CYAN);

        message.replyEmbeds(messageEmbed.build())
                .queue(m -> eventWaiter.waitForEvent(
                        GuildMessageReceivedEvent.class,
                        messageEvent -> {
                            if (isInvalidAuthor(textChannel, messageEvent, message.getAuthor())) return false;

                            var messageFromEvent = messageEvent.getMessage();
                            var monthsToAdd = messageFromEvent.getContentRaw();

                            if (isValidAmountOfMonths(monthsToAdd)) {
                                return true;
                            } else {
                                var title = messageService.getMessage(INVALID_MONTHS);
                                messageFromEvent.replyEmbeds(embedService.getInvalidParameterEmbed(title)).queue();
                                addErrorReaction(messageFromEvent);

                                return false;
                            }
                        },
                        messageEvent -> {
                            var messageFromEvent = messageEvent.getMessage();
                            var monthsToAdd = Integer.parseInt(messageFromEvent.getContentRaw());
                            var serverID = serverService.findServerIDByServerNameOrServerID(server).orElse(0);

                            messageFromEvent
                                    .replyEmbeds(sourceBanPlayerService.renewPlayerExpirationDate(monthsToAdd, steamID, serverID))
                                    .queue();

                            removeUserFromSet(author.getId());
                        },
                        90, TimeUnit.SECONDS,
                        () -> {
                            removeUserFromSet(author.getId());
                            pingUserIfTimeIsOut(textChannel, author);
                        }
                ));
    }

    private void pingUserIfTimeIsOut(TextChannel textChannel, User author) {
        var message = String.format(messageService.getMessage(EVENT_WAITER_TIMEOUT), author.getId());
        textChannel.sendMessage(message).queue();
    }

    private void addSuccessReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(SUCCESS_REACTION).queue();
    }

    private void addErrorReaction(Message messageFromEvent) {
        messageFromEvent.addReaction(ERROR_REACTION).queue();
    }
}
