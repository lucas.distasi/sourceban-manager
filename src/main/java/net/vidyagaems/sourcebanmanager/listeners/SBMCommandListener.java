package net.vidyagaems.sourcebanmanager.listeners;

import lombok.RequiredArgsConstructor;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vidyagaems.sourcebanmanager.services.DiscordRolesService;
import net.vidyagaems.sourcebanmanager.services.EmbedService;
import net.vidyagaems.sourcebanmanager.services.MessageService;
import org.springframework.stereotype.Component;

import static net.vidyagaems.sourcebanmanager.constants.Constants.PropertiesConstants.INVALID_PARAMETER_PROPERTY;
import static net.vidyagaems.sourcebanmanager.constants.Constants.SBM_COMMAND;
import static net.vidyagaems.sourcebanmanager.utils.UserEventUtils.isExecutingCommand;
import static net.vidyagaems.sourcebanmanager.utils.Utils.getRoles;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isSBMCommand;
import static net.vidyagaems.sourcebanmanager.utils.Utils.isSBMCommandWithMenuFromStaff;

@Component
@RequiredArgsConstructor
public class SBMCommandListener extends ListenerAdapter {

    private final EmbedService embedService;
    private final MessageService messageService;
    private final DiscordRolesService discordRolesService;

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        var message = event.getMessage();
        var member = event.getGuild().getMember(message.getAuthor());

        if (isSBMCommand(message.getContentRaw()) &&
                !isExecutingCommand(message.getAuthor().getId()) &&
                discordRolesService.hasStaffRole(getRoles(member))) {
            message.replyEmbeds(embedService.getSBMMessageEmbed()).queue();
        }

        if (message.getContentRaw().startsWith(SBM_COMMAND)) {
            var commandWithParameters = message.getContentRaw().split(" ");

            if (isSBMCommandWithMenuFromStaff(commandWithParameters) &&
                    !isExecutingCommand(message.getAuthor().getId()) &&
                    discordRolesService.hasStaffRole(getRoles(member))) {
                var parameter = commandWithParameters[1];
                var allEmbeds = embedService.getAllEmbeds();
                var error = String.format(messageService.getMessage(INVALID_PARAMETER_PROPERTY), parameter);
                var messageEmbed = allEmbeds
                        .getOrDefault(parameter.toLowerCase(),
                                embedService.getErrorMessageEmbed(error));

                message.replyEmbeds(messageEmbed).queue();
            }
        }
    }
}
