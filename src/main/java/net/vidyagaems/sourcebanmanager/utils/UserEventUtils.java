package net.vidyagaems.sourcebanmanager.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserEventUtils {

    private static final Set<String> USERS = new HashSet<>();

    public static boolean isExecutingCommand(String userID) {
        return USERS.contains(userID);
    }

    public static void removeUserFromSet(String userID) {
        USERS.remove(userID);
    }

    public static void addUserToSet(String userID) {
        USERS.add(userID);
    }
}
