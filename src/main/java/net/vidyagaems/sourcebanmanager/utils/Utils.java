package net.vidyagaems.sourcebanmanager.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.models.PlayerType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static net.vidyagaems.sourcebanmanager.constants.Constants.SBM_COMMAND;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {

    private static final List<String> PARAMETERS_WITH_MENU = List.of("bans", "comms", "staff", "vips", "help");

    public static boolean isSBMCommandWithMenuFromStaff(String[] message) {
        return message.length == 2 &&
                isSBMCommand(message[0]) &&
                isParameterWithMenu(message[1]);
    }

    public static List<String> getRoles(Member member) {
        return member == null ?
                Collections.emptyList() :
                member.getRoles().stream()
                        .map(Role::getId)
                        .collect(Collectors.toList());
    }

    public static boolean isSBMCommand(String message) {
        return SBM_COMMAND.equalsIgnoreCase(message);
    }

    private static boolean isParameterWithMenu(String parameter) {
        return PARAMETERS_WITH_MENU.contains(parameter);
    }

    public static boolean isValidSteamID(String steamID) {
        return steamID.matches("^STEAM_[0-5]:[01]:\\d{5,12}+$");
    }

    public static boolean isValidUsername(String username) {
        return StringUtils.isNotBlank(username) && username.matches("[a-zA-Z ]{2,15}");
    }

    public static boolean isValidServer(String server, List<Server> servers) {
        return CollectionUtils.isNotEmpty(servers) &&
                servers.stream()
                        .anyMatch(s -> Normalizer.normalize(server, Normalizer.Form.NFC).equalsIgnoreCase(s.getServerName()) ||
                                Normalizer.normalize(server, Normalizer.Form.NFC).equalsIgnoreCase(String.valueOf(s.getServerID())));
    }

    public static boolean isValidTagName(String tagName) {
        return (StringUtils.isNotBlank(tagName) &&  tagName.matches("[a-zA-Z ]{2,15}")) ||
                "S".equalsIgnoreCase(tagName);
    }

    public static boolean isValidHexColor(String color) {
        return color.matches("[a-fA-F0-9]{6}$");
    }

    public static boolean isValidDiscordID(String discordID) {
        return "0".equals(discordID) ||
                (discordID.length() <= 20 && discordID.length() >= 13 &&
                StringUtils.isNumeric(discordID) && Long.parseLong(discordID) >= 0);
    }

    public static boolean isValidAmountOfMonths(String months) {
        return months.length() < 2 &&
                StringUtils.isNumeric(months) &&
                Integer.parseInt(months) > 0;
    }

    public static boolean isInvalidAuthor(TextChannel textChannel, GuildMessageReceivedEvent event, User author) {
        return !event.getChannel().getId().equals(textChannel.getId()) ||
                author.getIdLong() != event.getAuthor().getIdLong() ||
                event.getAuthor().isBot();
    }

    public static boolean isValidConfirmationMessage(String message) {
        return isAffirmativeConfirmationMessage(message) ||
                "No".equalsIgnoreCase(message) ||
                "N".equalsIgnoreCase(message);
    }

    public static boolean isAffirmativeConfirmationMessage(String message) {
        return "Y".equalsIgnoreCase(message) ||
                "Yes".equalsIgnoreCase(message) ||
                "S".equalsIgnoreCase(message) ||
                "Si".equalsIgnoreCase(message);
    }

    public static String getPlayerType(String command) {
        if (PlayerType.MOD.getCommands().contains(command.toLowerCase())) {
            return PlayerType.MOD.name();
        }

        if (PlayerType.ADMIN.getCommands().contains(command.toLowerCase())) {
            return PlayerType.ADMIN.name();
        }

        return PlayerType.VIP.name();
    }
}
