package net.vidyagaems.sourcebanmanager.config;

import net.vidyagaems.sourcebanmanager.entities.ServerGroup;
import net.vidyagaems.sourcebanmanager.logger.cache.CacheLogger;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheEventListenerConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.event.EventType;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;
import javax.cache.Caching;
import java.time.Duration;
import java.util.List;
import java.util.Map;

@Configuration
@EnableCaching
public class EhcacheConfig {

    private static final Duration DURATION_72_HOURS = Duration.ofHours(72);
    private static final Duration DURATION_6_HOURS = Duration.ofHours(6);

    @Bean
    public CacheManager getCacheManager() {
        var provider = Caching.getCachingProvider();
        var cacheManager = provider.getCacheManager();
        var caches = getAllCaches();

        var asynchronousListener = CacheEventListenerConfigurationBuilder
                .newEventListenerConfiguration(new CacheLogger(), EventType.CREATED, EventType.EXPIRED)
                .unordered()
                .asynchronous();

        caches.forEach((cacheName, cacheConfig) -> cacheManager.createCache(
                cacheName,
                Eh107Configuration.fromEhcacheCacheConfiguration(cacheConfig.withService(asynchronousListener))
        ));

        return cacheManager;
    }

    private Map<String, CacheConfigurationBuilder<?, ?>> getAllCaches() {
        return Map.of(
                "findAllServers", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                SimpleKey.class,
                                List.class,
                                ResourcePoolsBuilder.heap(10).offheap(10, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "findAllServerGroups", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                SimpleKey.class,
                                List.class,
                                ResourcePoolsBuilder.heap(10).offheap(10, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "findServerNameByServerIDOrServerName", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                String.class,
                                String.class,
                                ResourcePoolsBuilder.heap(10).offheap(10, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "findServerIDByServerNameOrServerID", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                String.class,
                                Integer.class,
                                ResourcePoolsBuilder.heap(10).offheap(10, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "getServerGroupName", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                Integer.class,
                                String.class,
                                ResourcePoolsBuilder.heap(10).offheap(10, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "findOutdatedPlayersRolesWithOffsetDays", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                Integer.class,
                                List.class,
                                ResourcePoolsBuilder.heap(100).offheap(25, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_6_HOURS)),

                "getServerGroupID", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                String.class,
                                Integer.class,
                                ResourcePoolsBuilder.heap(10).offheap(25, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS)),

                "getServerGroup", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                String.class,
                                ServerGroup.class,
                                ResourcePoolsBuilder.heap(10).offheap(25, MemoryUnit.MB))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(DURATION_72_HOURS))
        );
    }

}
