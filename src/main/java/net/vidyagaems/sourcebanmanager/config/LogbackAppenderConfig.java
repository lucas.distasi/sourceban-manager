package net.vidyagaems.sourcebanmanager.config;

import ch.qos.logback.classic.LoggerContext;
import lombok.RequiredArgsConstructor;
import net.vidyagaems.sourcebanmanager.appenders.ErrorAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class LogbackAppenderConfig {

    private final ApplicationContext applicationContext;

    @Bean
    public void logbackAppender() {
        var loggerFactory = (LoggerContext) LoggerFactory.getILoggerFactory();
        var errorAppender = applicationContext.getBean(ErrorAppender.class);
        var rootLogger = loggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

        rootLogger.addAppender(errorAppender);
    }
}
