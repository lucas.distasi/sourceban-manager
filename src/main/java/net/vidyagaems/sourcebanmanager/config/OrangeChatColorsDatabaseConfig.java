package net.vidyagaems.sourcebanmanager.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "orangeChatColorsEntityManagerFactory",
        transactionManagerRef = "orangeChatColorsTransactionManager",
        basePackages = "net.vidyagaems.sourcebanmanager.repositories.chatcolors.orange"
)
public class OrangeChatColorsDatabaseConfig {

    @Bean(name = "orangeChatColorsDataSource")
    @ConfigurationProperties(prefix = "chatcolors.orange.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .build();
    }

    @Bean(name = "orangeChatColorsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean orangeChatColorsEntityManagerFactory(EntityManagerFactoryBuilder orangeChatColorsEntityManagerFactory,
                                                                                        @Qualifier("orangeChatColorsDataSource") DataSource dataSource) {
        return orangeChatColorsEntityManagerFactory
                .dataSource(dataSource)
                .packages("net.vidyagaems.sourcebanmanager.entities")
                .build();
    }

    @Bean(name = "orangeChatColorsTransactionManager")
    public PlatformTransactionManager orangeTransactionManager(@Qualifier("orangeChatColorsEntityManagerFactory")
                                                                        EntityManagerFactory orangeChatColorsEntityManagerFactory) {
        return new JpaTransactionManager(orangeChatColorsEntityManagerFactory);
    }
}
