package net.vidyagaems.sourcebanmanager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Configuration
public class LocaleResolverConfig {

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        LocaleContextHolder.setDefaultLocale(new Locale("es", "ES"));
        localeResolver.setDefaultLocale(Locale.forLanguageTag("es_ES"));

        return localeResolver;
    }
}
