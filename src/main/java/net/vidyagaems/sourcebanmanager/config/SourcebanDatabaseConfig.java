package net.vidyagaems.sourcebanmanager.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "sourcebanEntityManagerFactory",
        transactionManagerRef = "sourcebanTransactionManager",
        basePackages = "net.vidyagaems.sourcebanmanager.repositories.sourceban"
)
public class SourcebanDatabaseConfig {

    @Primary
    @Bean(name = "sourcebanDataSource")
    @ConfigurationProperties(prefix = "sourceban.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .build();
    }

    @Primary
    @Bean(name = "sourcebanEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean sourcebanEntityManagerFactory(EntityManagerFactoryBuilder sourcebanEntityManagerFactory,
                                                                             @Qualifier("sourcebanDataSource") DataSource dataSource) {
        return sourcebanEntityManagerFactory
                .dataSource(dataSource)
                .packages("net.vidyagaems.sourcebanmanager.entities")
                .build();
    }

    @Primary
    @Bean(name = "sourcebanTransactionManager")
    public PlatformTransactionManager sourcebanTransactionManager(@Qualifier("sourcebanEntityManagerFactory")
                                                                       EntityManagerFactory sourcebanEntityManagerFactory) {
        return new JpaTransactionManager(sourcebanEntityManagerFactory);
    }
}
