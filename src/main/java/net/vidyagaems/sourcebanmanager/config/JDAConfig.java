package net.vidyagaems.sourcebanmanager.config;

import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.security.auth.login.LoginException;
import java.util.List;

@Component
public class JDAConfig {

    @Value("${discord.bot.token}")
    private String token;

    private final EventWaiter eventWaiter = new EventWaiter();

    @Bean
    public JDA getJda(List<EventListener> eventListeners) throws LoginException, InterruptedException {
        return JDABuilder.createDefault(token)
                .addEventListeners(eventListeners.toArray())
                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .setChunkingFilter(ChunkingFilter.ALL)
                .enableIntents(GatewayIntent.GUILD_MEMBERS)
                .build()
                .awaitReady();
    }

    @Bean
    public EventWaiter eventWaiter() {
        return eventWaiter;
    }
}
