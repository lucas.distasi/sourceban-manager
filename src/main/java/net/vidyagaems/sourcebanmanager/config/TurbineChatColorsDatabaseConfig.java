package net.vidyagaems.sourcebanmanager.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "turbineChatColorsEntityManagerFactory",
        transactionManagerRef = "turbineChatColorsTransactionManager",
        basePackages = "net.vidyagaems.sourcebanmanager.repositories.chatcolors.turbine"
)
public class TurbineChatColorsDatabaseConfig {

    @Bean(name = "turbineChatColorsDataSource")
    @ConfigurationProperties(prefix = "chatcolors.turbine.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .build();
    }

    @Bean(name = "turbineChatColorsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean turbineChatColorsEntityManagerFactory(EntityManagerFactoryBuilder turbineChatColorsEntityManagerFactory,
                                                                              @Qualifier("turbineChatColorsDataSource") DataSource dataSource) {
        return turbineChatColorsEntityManagerFactory
                .dataSource(dataSource)
                .packages("net.vidyagaems.sourcebanmanager.entities")
                .build();
    }

    @Bean(name = "turbineChatColorsTransactionManager")
    public PlatformTransactionManager turbineTransactionManager(@Qualifier("turbineChatColorsEntityManagerFactory")
                                                                        EntityManagerFactory turbineChatColorsEntityManagerFactory) {
        return new JpaTransactionManager(turbineChatColorsEntityManagerFactory);
    }
}
