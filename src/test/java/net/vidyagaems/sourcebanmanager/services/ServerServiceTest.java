package net.vidyagaems.sourcebanmanager.services;

import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.entities.Server;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.ServerRepository;
import net.vidyagaems.sourcebanmanager.services.impl.ServerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static net.vidyagaems.sourcebanmanager.constants.Constants.CommandsConstants.UNKNOWN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ServerServiceTest {

    @Mock
    ServerRepository serverRepositoryMock;

    @Mock
    AdminServerGroupService adminServerGroupServiceMock;

    ServerServiceImpl serverService;

    @BeforeEach
    void setUp() {
        serverService = new ServerServiceImpl(serverRepositoryMock, adminServerGroupServiceMock);
    }

    @Test
    void shouldRetrieveAllServers() {
        when(serverRepositoryMock.findAll()).thenReturn(getAllServers());

        var servers = serverService.findAll();

        verify(serverRepositoryMock, times(1)).findAll();
        assertThat(servers.size(), is(1));
    }

    @Test
    void shouldReturnUnknownServersForAdminID() {
        when(adminServerGroupServiceMock.findPlayerServerGroups(1))
                .thenReturn(Optional.of(getAdminServerGroups()));

        var servers = serverService.findServersForAdminID(1);

        verify(adminServerGroupServiceMock, times(1)).findPlayerServerGroups(1);
        assertThat(servers.size(), is(1));
        assertThat(servers.get(0).getServerID(), is(1));
        assertThat(servers.get(0).getServerName(), is(UNKNOWN));
    }

    private List<Server> getAllServers() {
        var server = new Server();
        server.setServerID(1);
        server.setServerName("One");

        return Collections.singletonList(server);
    }

    private List<AdminServerGroup> getAdminServerGroups() {
        var adminServerGroup = new AdminServerGroup();
        adminServerGroup.setAdminID(1);
        adminServerGroup.setGroupID(1);
        adminServerGroup.setServerGroupID(-1);
        adminServerGroup.setServerID(1);

        return Collections.singletonList(adminServerGroup);
    }

}