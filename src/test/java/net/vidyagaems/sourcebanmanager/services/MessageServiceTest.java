package net.vidyagaems.sourcebanmanager.services;

import net.vidyagaems.sourcebanmanager.services.impl.MessageServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {

    @Mock
    MessageSource messageSourceMock;

    MessageServiceImpl messageService;

    @BeforeEach
    void setUp() {
        messageService = new MessageServiceImpl(messageSourceMock);
    }

    @Test
    void shouldRetrieveMessage() {
        var property = "some-property";

        when(messageSourceMock.getMessage(property, null, LocaleContextHolder.getLocale()))
                .thenReturn("some value");

        var message = messageService.getMessage("some-property");

        assertThat(message, is("some value"));
    }

}