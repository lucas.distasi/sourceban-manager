package net.vidyagaems.sourcebanmanager.services;

import net.vidyagaems.sourcebanmanager.entities.AdminServerGroup;
import net.vidyagaems.sourcebanmanager.repositories.sourceban.AdminServerGroupRepository;
import net.vidyagaems.sourcebanmanager.services.impl.AdminServerGroupServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AdminServerGroupServiceTest {

    @Mock
    AdminServerGroupRepository adminServerGroupRepositoryMock;

    AdminServerGroupServiceImpl adminServerGroupService;
    AdminServerGroup adminServerGroup = new AdminServerGroup();

    @BeforeEach
    void setUp() {
        adminServerGroupService = new AdminServerGroupServiceImpl(adminServerGroupRepositoryMock);
    }


    @Test
    void shouldCallSaveOnce() {
        adminServerGroupService.save(adminServerGroup);

        verify(adminServerGroupRepositoryMock, times(1))
                .save(adminServerGroup);
    }

    @Test
    void shouldCallFindByAdminIDOrderByServerIDOnce() {
        var adminID = 1;

        adminServerGroupService.findPlayerServerGroups(adminID);

        verify(adminServerGroupRepositoryMock, times(1))
                .findByAdminIDOrderByServerID(adminID);
    }

    @Test
    void shouldCallFindAdminServerGroupByAdminIDAndServerIDOnce() {
        var adminID = 1;
        var serverID = 1;

        adminServerGroupService.findAdminServerGroupByAdminIDAndServerID(adminID, serverID);

        verify(adminServerGroupRepositoryMock, times(1))
                .findAdminServerGroupByAdminIDAndServerID(adminID, serverID);
    }

    @Test
    void shouldCallDeleteAllByAdminIDAndServerIDOnce() {
        var adminID = 1;
        var serverID = 1;

        adminServerGroupService.deleteAllByAdminIDAndServerID(adminID, serverID);

        verify(adminServerGroupRepositoryMock, times(1))
                .deleteAllByAdminIDAndServerID(adminID, serverID);
    }
}