    CREATE TABLE sb_players_registry (
        id INT AUTO_INCREMENT PRIMARY KEY,
        aid INT NOT NULL,
        steamid VARCHAR(30) NOT NULL,
        username VARCHAR(30) NOT NULL,
        role VARCHAR(30) NOT NULL,
        dateAdded DATE NOT NULL,
        dateRemoved DATE
    );
---
    CREATE TRIGGER save_player_into_sb_players_registry
    AFTER INSERT
    ON sb_admins
    FOR EACH ROW
        BEGIN
            IF (NEW.srv_group <> 'VIP') THEN
                INSERT INTO sb_players_registry(steamid, role, username, dateAdded)
                VALUES (NEW.authid, NEW.srv_group, NEW.user, NOW());
            END IF;
        END;
---
    CREATE TRIGGER update_sb_players_registry_date_removed
    AFTER DELETE
    ON sb_admins
    FOR EACH ROW
        BEGIN
            IF (OLD.srv_group <> 'VIP') THEN
                UPDATE sb_players_registry
                SET dateRemoved = NOW()
                WHERE OLD.aid = aid
                AND OLD.authid = steamid;
            END IF;
        END;